<?php


 Auth::routes();
 Route::group(['middleware'=>['auth']], function(){

	//Home Routes
	Route::get('/', 'HomeController@index')->name('home');

	//Users Routes
	Route::get('/users', 'UsersController@index')->name('users');
	Route::post('/users/search', 'UsersController@search')->name('userSearch');
	Route::get('/users/activation/{id}', 'UsersController@activation')->name('userActivation');
	Route::get('/users/{id}', 'UsersController@show')->name('user');
	
	//Driver Routes
	Route::get('/drivers', 'DriversController@index')->name('drivers');
	Route::get('/drivers/indexApplicants', 'DriversController@indexApplicants')->name('indexApplicants');
	Route::post('/drivers/search', 'DriversController@search')->name('driverSearch');
	Route::get('/drivers/activation/{id}', 'DriversController@activation')->name('driverActivation');
	Route::get('/drivers/apply/{id}', 'DriversController@apply')->name('driverApply');
      Route::get('/drivers/deleteApplicant/{id}/{adminMessage}', 'DriversController@deleteApplicant')->name('deleteApplicant');
	Route::get('/drivers/editRequest', 'DriversController@editRequest')->name('editRequest');
	Route::get('/drivers/{id}', 'DriversController@show')->name('driver');
      Route::get('/drivers/acceptEditRequest/{id}/{key}/', 'DriversController@acceptEditRequest')->name('acceptEditRequest');
      Route::get('/drivers/refuseEditRequest/{id}/{key}/{value}', 'DriversController@refuseEditRequest')->name('refuseEditRequest');
   

	//Order Routes
	Route::get('/orders', 'OrdersController@index')->name('orders');
	Route::get('/orders/{id}', 'OrdersController@show')->name('user');
      Route::get('/orders/printView/{id}', 'OrdersController@printView')->name('printView');
	Route::get('/orders/changeOrderStatus/{status}/{id}', 'OrdersController@changeOrderStatus')->name('changeOrderStatus');
	Route::get('/orders/orderDestroy/{id}', 'OrdersController@orderDestroy')->name('orderDestroy');
	
	//desserts Routes
	Route::get('/desserts', 'DessertsController@index')->name('desserts');
	Route::post('/desserts/search', 'DessertsController@search')->name('dessertSearch');
	Route::get('/desserts/dessertAdd', 'DessertsController@dessertAdd')->name('dessertAdd');
	Route::post('/desserts/dessertCreate', 'DessertsController@dessertCreate')->name('dessertCreate');
	Route::get('/desserts/dessertEdit/{id}', 'DessertsController@dessertEdit')->name('dessertEdit');
	Route::post('/desserts/dessertUpdate/{id}', 'DessertsController@dessertUpdate')->name('dessertUpdate');
	Route::get('/desserts/dessertDestroy/{id}', 'DessertsController@dessertDestroy')->name('dessertDestroy');
    
    
	//Extra Routes
	Route::get('/desserts/dessertExtra', 'DessertsController@dessertExtra')->name('dessertExtra');

	//addon Routes
	Route::get('/desserts/addonAdd', 'DessertsController@addonAdd')->name('addonAdd');
	Route::post('/desserts/addonCreate', 'DessertsController@addonCreate')->name('addonCreate');
	Route::get('/desserts/addonEdit/{id}', 'DessertsController@addonEdit')->name('addonEdit');
	Route::put('/desserts/addonUpdate/{id}', 'DessertsController@addonUpdate')->name('addonUpdate');
	Route::get('/desserts/addonDestroy/{id}', 'DessertsController@addonDestroy')->name('addonDestroy');
    

	//cake Routes
	Route::get('/desserts/cakeAdd', 'DessertsController@cakeAdd')->name('cakeAdd');
	Route::post('/desserts/cakeCreate', 'DessertsController@cakeCreate')->name('cakeCreate');
	Route::get('/desserts/cakeEdit/{id}', 'DessertsController@cakeEdit')->name('cakeEdit');
	Route::put('/desserts/cakeUpdate/{id}', 'DessertsController@cakeUpdate')->name('cakeUpdate');
	Route::get('/desserts/cakeDestroy/{id}', 'DessertsController@cakeDestroy')->name('cakeDestroy');

	//sauce Routes
	Route::get('/desserts/sauceAdd', 'DessertsController@sauceAdd')->name('sauceAdd');
	Route::post('/desserts/sauceCreate', 'DessertsController@sauceCreate')->name('sauceCreate');
	Route::get('/desserts/sauceEdit/{id}', 'DessertsController@sauceEdit')->name('sauceEdit');
	Route::put('/desserts/sauceUpdate/{id}', 'DessertsController@sauceUpdate')->name('sauceUpdate');
	Route::get('/desserts/sauceDestroy/{id}', 'DessertsController@sauceDestroy')->name('sauceDestroy');
    
    // Dessert Size Routes
    Route::get('/desserts/dessertShow/{id}','DessertsController@dessertShow')->name('dessertShow');
    Route::get('/desserts/dessertAddSize/{id}','DessertsController@dessertAddSize')->name('dessertAddSize');
    Route::post('/desserts/dessertCreateSize/{id}','DessertsController@dessertCreateSize')->name('dessertCreateSize');
    Route::get('/desserts/dessertSizeDelete/{id}','DessertsController@dessertSizeDelete')->name('dessertSizeDelete');
    Route::get('/desserts/dessertSizeDefault/{dessertId}/{dessertSizeId}','DessertsController@dessertSizeDefault')->name('dessertSizeDefault');
    //Route::get('/desert/edit/{dessertId}/{dessertSizeId}','DessertsController@desertsSizeedit');
    Route::post('/desertsSize/update/{dessertId}/{dessertSizeId}','DessertsController@desertsSizeupdate');
    
   Route::get('/desert/edit/{id}','DessertsController@desertsSizeedit');
  //  Route::post('/desertsSize/update/{id}','DessertsController@desertsSizeupdate');
    
    // Dessert Promotion Routes
    Route::get('/desserts/dessertPromotionAdd/{dessertId}/{dessertSizeId}','DessertsController@dessertPromotionAdd')->name('dessertPromotionAdd');
    Route::post('/desserts/dessertPromotionCreate/{dessertId}/{dessertSizeId}','DessertsController@dessertPromotionCreate')->name('dessertPromotionCreate');
    Route::get('/desserts/dessertPromotionEdit/{dessertSizeId}','DessertsController@dessertPromotionEdit')->name('dessertPromotionEdit');
    Route::post('/desserts/dessertPromotionUpdate/{dessertSizeId}','DessertsController@dessertPromotionUpdate')->name('dessertPromotionUpdate');
    Route::get('/desserts/dessertPromotionDestroy/{dessertSizeId}','DessertsController@dessertPromotionDestroy')->name('dessertPromotionDestroy');
    
    
    Route::get('/desert/prmotion/{id}','DessertsController@desertPrmmtion');

	//branches Routes
	Route::get('/branches', 'BranchController@index')->name('branches');
	Route::get('/branches/branchAdd', 'BranchController@branchAdd')->name('branchAdd');
	Route::post('/branches/branchCreate', 'BranchController@branchCreate')->name('branchCreate');
	Route::get('/branches/branchEdit/{id}', 'BranchController@branchEdit')->name('branchEdit');
	Route::put('/branches/branchUpdate/{id}', 'BranchController@branchUpdate')->name('branchUpdate');
	Route::get('/branches/branchDestroy/{id}', 'BranchController@branchDestroy')->name('branchDestroy');

	//cities Routes
	Route::get('/branches/cities', 'BranchController@indexCities')->name('cities');
	Route::get('/branches/cityAdd', 'BranchController@cityAdd')->name('cityAdd');
	Route::post('/branches/cityCreate', 'BranchController@cityCreate')->name('cityCreate');
	Route::get('/branches/cityEdit/{id}', 'BranchController@cityEdit')->name('cityEdit');
	Route::put('/branches/cityUpdate/{id}', 'BranchController@cityUpdate')->name('cityUpdate');
	Route::get('/branches/cityDestroy/{id}', 'BranchController@cityDestroy')->name('cityDestroy');
    Route::get('/branches/listCityTowns/{id}', 'BranchController@listCityTowns')->name('listCityTowns');

	//towns Routes
	Route::get('/branches/towns', 'BranchController@indexTowns')->name('towns');
	Route::get('/branches/townAdd', 'BranchController@townAdd')->name('townAdd');
	Route::post('/branches/townCreate', 'BranchController@townCreate')->name('townCreate');
	Route::get('/branches/townEdit/{id}', 'BranchController@townEdit')->name('townEdit');
	Route::put('/branches/townUpdate/{id}', 'BranchController@townUpdate')->name('townUpdate');
	Route::get('/branches/townDestroy/{id}', 'BranchController@townDestroy')->name('townDestroy');

	//markets Routes
	Route::get('/markets', 'MarketController@index')->name('markets');
	Route::get('/markets/marketAdd', 'MarketController@marketAdd')->name('marketAdd');
	Route::post('/markets/marketCreate', 'MarketController@marketCreate')->name('marketCreate');
	Route::get('/markets/marketEdit/{id}', 'MarketController@marketEdit')->name('marketEdit');
	Route::put('/markets/marketUpdate/{id}', 'MarketController@marketUpdate')->name('marketUpdate');
	Route::get('/markets/marketDestroy/{id}', 'MarketController@marketDestroy')->name('marketDestroy');
    Route::get('/markets/listMarketDessert/{id}', 'MarketController@listMarketDessert')->name('listMarketDessert');
    Route::get('/markets/addMarketDessert/{MarketId}/{DessertId}', 'MarketController@addMarketDessert')->name('addMarketDessert');
    Route::get('/markets/removeMarketDessert/{MarketId}/{DessertId}', 'MarketController@removeMarketDessert')->name('removeMarketDessert');



	//notification Routes  ,,,,, omar yehia
	Route::get('/notification', 'NotificationController@index')->name('notification');
	Route::post('/sendPushNotification', 'NotificationController@send')->name('notification.send');
	
	Route::post('/getPersonInfo', 'NotificationController@getPersonInfo')->name('getPersonInfo');
	
    // Route::get('/notification/notificationDestroy/{id}', 'NotificationController@notificationDestroy')->name('notificationDestroy');
    // Route::post('/notification/notificationSearch', 'NotificationController@notificationSearch')->name('notificationSearch');
    
	//complain Routes
	Route::get('/complain', 'ComplainController@index')->name('complain');
    Route::get('/complain/complainDestroy/{id}', 'ComplainController@complainDestroy')->name('complainDestroy');
    Route::post('/complain/complainSearch', 'ComplainController@complainSearch')->name('complainSearch');

	//contact Routes
	Route::get('/contact', 'ContactController@contactIndex')->name('contact');
	Route::get('/contact/emailEdit/{id}', 'ContactController@emailEdit')->name('emailEdit');
	Route::put('/contact/emailUpdate/{id}', 'ContactController@emailUpdate')->name('emailUpdate');
	Route::get('/contact/phoneAdd', 'ContactController@phoneAdd')->name('phoneAdd');
	Route::post('/contact/phoneCreate', 'ContactController@phoneCreate')->name('phoneCreate');
	Route::get('/contact/phoneDestroy/{id}', 'ContactController@phoneDestroy')->name('phoneDestroy');

	//about Routes
	Route::get('/about', 'ContactController@aboutIndex')->name('about');
	Route::get('/about/aboutEdit/{id}', 'ContactController@aboutEdit')->name('aboutEdit');
	Route::put('/about/aboutUpdate/{id}', 'ContactController@aboutUpdate')->name('aboutUpdate'); 
	
	
	Route::get('/fees', 'ContactController@feesIndex')->name('fees');
	Route::get('/fees/feesEdit/{id}', 'ContactController@feesEdit')->name('feesEdit');
	Route::put('/fees/feesUpdate/{id}', 'ContactController@feesUpdate')->name('feesUpdate'); });
	
	Route::get('/discount_codes', 'discount_codesController@index');
	Route::get('/discount_codes/create', 'discount_codesController@create');
	Route::post('/discount_codes/store', 'discount_codesController@store')->name('discount_codes.store');
	Route::get('/discount_codes/edit/{id}', 'discount_codesController@edit');
	Route::post('/discount_codes/update', 'discount_codesController@update')->name('discount_codes.update');
	Route::get('/discount_codes/activation/{id}', 'discount_codesController@activation');
	Route::get('/discount_codes/destroy/{id}', 'discount_codesController@destroy');




 Route::get('/logout', function(){ Auth::logout(); return redirect("/login"); });
