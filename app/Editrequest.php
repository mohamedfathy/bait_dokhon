<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Driver;

class Editrequest extends Model
{
	
    public $table='editRequests';
    public function driver () {
    	return $this->belongsTo('App\Driver', 'drivers_id');
    }
}
