<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dessertpromotion extends Model
{
    //
    
    public $table="dessertPromotions";
    public $timestamps=false;
    protected $primaryKey = 'dessertSizes_id';
    public function Dessertsize(){
        return $this->belongsTo('\App\Dessertsize','dessertSizes_id');
    }
}
