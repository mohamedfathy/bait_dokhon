<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\City;
use App\Dessert;

class Market extends Model
{
    //
    public $timestamps = false;

    public function city () {
    	return $this->belongsTo('App\City', 'cities_id', 'cityId');
    }
    
    public function desserts()
    {
        return $this->belongsToMany('App\Dessert', 'markets_desserts', 'markets_id', 'desserts_id');
    }
}
