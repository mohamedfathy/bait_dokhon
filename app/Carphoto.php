<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carphoto extends Model
{
 
 protected $table = 'carPhotos';
    public $timestamps = false;
    
}
