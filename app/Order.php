<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Branch; 

class Order extends Model
{
    //
    public $timestamps = false;
    
    public function user () {
    	return $this->belongsTo('App\User', 'users_id', 'id');
    }

    public function branch () {
    	return $this->belongsTo('App\Branch', 'branches_id', 'id');
    }
     public function Deliveryorder () {
    	return $this->hasMany('App\Deliveryorder', 'orders_id');
    }
      public function specialOrders () {
    	return $this->hasMany('App\Specialorder', 'orders_id');
    }
    
     public function Normalorder () {
    	return $this->hasMany('App\Normalorder', 'orders_id');
    }
    public function Refusedorder () {
    	return $this->hasMany('App\Refusedorder', 'orders_id');
    }
    
    
}

