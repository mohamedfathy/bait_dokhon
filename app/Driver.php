<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Editrequest;
use App\City;

class Driver extends Model
{
    //
    protected $table = 'drivers';
    public $timestamps = false;

	// relationships
    public function editRequests () {
    	return $this->hasMany('App\Editrequest', 'drivers_id', 'id');
    }
    
    public function city()
    {
        return $this->belongsTo('App\City', 'cities_id', 'cityId');
    }
       public function  Carphoto(){
  return $this->hasMany('App\Carphoto','drivers_id');
   
}

}
