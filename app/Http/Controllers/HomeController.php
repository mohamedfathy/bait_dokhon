<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Driver;
use App\Order;
use Charts;

class HomeController extends Controller
{
    //
	public function index () {

        $users =  User::get()->count();
        $drivers =  Driver::get()->count();
        $orders =  Order::get()->count();


        $recent_users = User::orderBy('created_at', 'desc')->take(10)->get();
        $recent_drivers = Driver::orderBy('created_at', 'desc')->take(10)->get();

        $chart_users = Charts::multiDatabase('areaspline', 'highcharts')
            ->title('المستخدمين')
            ->colors(['green', '#ffffff'])
            ->dataset('المستخدمين', User::all())
            ->elementLabel("عدد المستخدمين")
            ->dimensions(0, 0)
            ->responsive(true)
            ->lastByDay();


        $chart_drivers = Charts::multiDatabase('areaspline', 'highcharts')
            ->title('السائقين')
            ->colors(['red', '#ffffff'])
            ->dataset('السائقين', Driver::all())
            ->elementLabel("عدد السائقين")
            ->dimensions(0, 0)
            ->responsive(true)
            ->lastByDay();

        $chart_orders = Charts::multiDatabase('areaspline', 'highcharts')
            ->title('الطلبات')
            ->colors(['blue', '#ffffff'])
            ->dataset('الطلبات', Order::all())
            ->dimensions(0, 0)
            ->responsive(true)
            ->elementLabel("عدد الطلبات")
            ->dateColumn('timestamp')
            ->lastByDay();
            
        return view('home.index', compact('users', 'drivers', 'orders', 'chart_users', 'chart_drivers', 'chart_orders', 'recent_users', 'recent_drivers'));
	} 
}
