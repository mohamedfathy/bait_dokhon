<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Market;
use Flashy;
use Validator;
use Exception;
use App\City;
use App\Marketdessert;
use App\Dessert;

class MarketController extends Controller
{
    //
	private $rules = [

    ];
    private $messages = [

    ];

    public function index () {
    	$Market = Market::all();
    	return view('markets.index', compact('Market'));
    }

    public function marketAdd () {
        $city=City::all();
    	return view('markets.add', compact('city'));
    }

    public function marketCreate (Request $request) {

        $rules = [
            'name_ar' =>'required|min:3|max:40',
            'name_en' =>'required|min:3|max:40',
            'latitude' =>'required',
            'logitude' =>'required',
            'image' =>'required|image',
            'cities_id'=>'required|integer',
        ];
        $messages = [
        	'required'  =>'لا بد من ادخال هذا الحقل' ,
        	'min'  =>' لا يمكن ان يقل  الاسم عن 3 حروف'  ,
            'max'  =>' لا يمكن ان يزيد  الاسم عن 40 حروف'  , 
            'image'  =>' يجب ادخال صورة حقيقية '  ,
        ];

       
        
        //Validate
      $errors = Validator::make($request->all(), $rules, $messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }


		$file=$request->image;
		$ad_photo = str_random(30) . '.' . $file->getClientOriginalExtension();
		$file->move( public_path('uploades/Markets') , $ad_photo);
		$url=Request()->root().'/uploades/Markets/'.$ad_photo;

        // Create New Dessert Record
        $Market = new Market;
        $Market->name_ar = $request->name_ar;
        $Market->name_en = $request->name_en;
        $Market->latitude = $request->latitude;
        $Market->logitude = $request->logitude;
        $Market->image = $url;
        $Market->cities_id =$request->cities_id;
        $handel = $Market->save();


        $msgSuccess = "تمت اضافة السوق بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة السوق";
        Flashy::success($handel ? $msgSuccess : $msgFailure);

        // Data to Return
    	$Market = Market::all();
    	return view('markets.index', compact('Market'));
    }

    public function marketEdit ($id) {
        $Market = Market::find($id);
        $city=City::all();
        return view('markets.edit', compact('Market','city'));
    }

    public function marketUpdate(Request $request){

        $rules = [
            'name_ar' =>'required|min:3|max:40',
            'name_en' =>'required|min:3|max:40',
            'latitude' =>'required',
            'logitude' =>'required',
            'image' =>'image',
            'city'=>'required',
        ];
        $messages = [
        	'required'  =>'لا بد من ادخال هذا الحقل' ,
        	'min'  =>' لا يمكن ان يقل  الاسم عن 3 حروف'  ,
            'max'  =>' لا يمكن ان يزيد  الاسم عن 40 حروف'  , 
            'image'  =>' يجب ادخال صورة حقيقية '  ,
        ];

        //Validate
      $errors = Validator::make($request->all(), $rules, $messages);
            if($errors->fails()) {

            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
     
        
      	if($request->image == true) {  
      	$file=$request->image;
		$ad_photo = str_random(30) . '.' . $file->getClientOriginalExtension();
		$file->move( public_path('uploades/Markets') , $ad_photo);
		$url=Request()->root().'/uploades/Markets/'.$ad_photo;
		}
        
   
        // Update Record
        $Market = Market::find($request->id);
        $Market->name_ar = $request->name_ar;
        $Market->name_en = $request->name_en;
        $Market->latitude = $request->latitude;
        $Market->logitude = $request->logitude;
        if($request->file('image') == true) {
          $Market->image = $url;
        }
        $Market->cities_id =$request->city;
        $handel = $Market->save();
        $msgSuccess = "تم تعديل المتجر بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل المتجر";
        Flashy::success($handel == true ? $msgSuccess : $msgFailure);
        // Data to Return
    	$Market = Market::all();
    	return view('markets.index', compact('Market'));
    }


    public function marketDestroy ($id) {
        $Market = false;
        try {
             $Market = Market::destroy($id);
        } catch (Exception $e) {
            $msgFailure = "لا يمكن الحذف  ... توجد بيانات مرتبطة بهذا الحقل.";
            Flashy::success($msgFailure);
            return redirect()->route('markets');
        }
       
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل ";
        Flashy::success($Market == true ? $msgSuccess : $msgFailure);
        return redirect()->route('markets');     
    }
    
    public function listMarketDessert ($id) {
    	$Marketdessert = Market::find($id);
        $Dessert = Dessert::all();
    	return view('markets.marketsDesserts', compact('Marketdessert', 'id', 'Dessert'));
    }

    public function addMarketDessert ($MarketId, $DessertId) {

        $Marketdessert = new Marketdessert;
        $Marketdessert->markets_id = $MarketId;
        $Marketdessert->desserts_id = $DessertId;
        $handel = $Marketdessert->save();


        $msgSuccess = "تمت اضافة الحلوى بالمتجر بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة الحلوى بالمتجر";
        Flashy::success($handel ? $msgSuccess : $msgFailure);

        // Get Redirected
        return back();
    }

    public function removeMarketDessert ($MarketId, $DessertId) {
        
         // Drop the record From Editrequest Table
         Marketdessert::where('markets_id', '=', $MarketId)->where('desserts_id', '=', $DessertId)->delete();
         
         // Get Redirected 
         return back();
    }
}
