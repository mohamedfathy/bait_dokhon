<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http;
use App\Driver;
use App\User;

class NotificationController extends Controller
{
   private $messages = [
        'required' => 'يجب ادخال قيمة',
        'numeric'  => 'يجب ادخال قيمة رقمية',
        'string'   => 'يجب ادخال قيمة نصية',
        'boolean'  => 'يجب ادخال قيمة منطقية',
        'unique'   => 'يجب ادخال قيمة مختلفه',
        'in'       => 'يجب ادخال قيمة محددة',
    ];
    
    // private $rules = [
    //     'content_ar'        => 'required|string',
    //     'content_en'        => 'required|string',
    //     ];
    
    private $pushNotificationRules = [
        'message'   => 'required|string',
        'TableName' => 'required_without:group|in:User,Driver',
        'phone'     => 'required_without:group|numeric',
        'group'     => 'required_without_all:TableName,phone|in:All,DriversOnly,UsersOnly',
        ];
        
    
    public function index () {
    // return "test Notification";
    return view('notifications.sendPushNotification');
    }
    
    public function getPersonInfo(Request $request) {
        $type_ar ='مندوب';
        $TableName ='Driver';
        $person = Driver::where('phone',$request->phone)->first();
        if($person){$photo = $person->photo;}
        if(is_null($person)){
            $person = User::where('phone',$request->phone)->first();
            $photo = 'uploads/user-avatar-saudi.png';
            $type_ar ='عميل';
            $TableName ='User';
        }
        if(!$person){return 'هذا الهاتف غير موجود';}
        else{
            return [
            // "id"=>$person->id,
            "name"=>$person->first_name.' '.$person->last_name,
            "phone"=>$person->phone,
            "photo"=>$photo,
            "type_ar"=>$type_ar,
            "TableName"=>$TableName
            // "firebaseToken"=>$person->firebase_token,
            ];
        }
    }
    
    public function send(Request $request){
        
      
        $this->validate($request, $this->pushNotificationRules, $this->messages);
        
        // Case#1 send to one person only
        if(isset($request->phone)){
            if($request->TableName === "Driver"){
                $person = Driver::where('phone',$request->phone)->first();
            }elseif($request->TableName ==="User"){
                $person = User::where('phone',$request->phone)->first();
            }
            
            if(!$person){
                return back()->with('error','حدث خطأ ما، لم يتم ارسال التنبيه');
                
            }

            $firebaseToken = $person->firebase_token;
                if($firebaseToken == null){            

                return back()->with('error','حدث خطأ ما، لم يتم ارسال التنبيه');
                    
                }
// dd($request->message);
        $result =  self::sendFCMEdit($request->message,$request->message,'specialNotification',$firebaseToken);


            if(json_decode($result)->success ==1){
                return back()->with('success','تم إرسال التنبيه بنجاح');
            }else{ 
                return back()->with('error','حدث خطأ ما، لم يتم ارسال التنبيه');
            }


        }
        //###########################################################

        // Case#2 send to a group
        if(isset($request->group)){
            switch ($request->group) {
                
                // teachers only
                case "DriversOnly":
                    $DriversFirebaseTokens = Driver::where('id' ,'>' ,0)->where('firebase_token','!=',null)->pluck('firebase_token')->toArray();
                    $UsersFirebaseTokens = null;
                    break;
                    
                // users only
                case "UsersOnly":
                    $UsersFirebaseTokens = User::where('id' ,'>' ,0)->where('firebase_token','!=',null)->pluck('firebase_token')->toArray();
                    $DriversFirebaseTokens = null;
                    break;
                    
                // All
                case "All":
                    $DriversFirebaseTokens = Driver::where('id' ,'>' ,0)->where('firebase_token','!=',null)->pluck('firebase_token')->toArray();
                    $UsersFirebaseTokens = User::where('id' ,'>' ,0)->where('firebase_token','!=',null)->pluck('firebase_token')->toArray();
            }
            
            // return [$UsersFirebaseTokens,$DriversFirebaseTokens];
            $errorsInSend = 0;
            if($DriversFirebaseTokens !== null){
                foreach($DriversFirebaseTokens as $firebaseToken){
                    // send push notification
                    $result =  self::sendFCMEdit($request->message,$request->message,'specialNotification',$firebaseToken);
                    if(json_decode($result)->success != 1){$errorsInSend++;}
              
                }                        
            }

            if($UsersFirebaseTokens !== null){
                foreach($UsersFirebaseTokens as $firebaseToken){
                    // send push notification
                    $result = self::sendFCMEdit($request->message,$request->message,'specialNotification',$firebaseToken);
                    if(json_decode($result)->success != 1){$errorsInSend++;}
                } 
            }
            

        }
        if($errorsInSend>0){return back()->with('success','تم إرسال التنبيهات مع وجود '.$errorsInSend.' خطأَ');}
        else{return back()->with('success','تم إرسال التنبيهات بنجاح');}
    }

    public static function sendFCM($mess,$fire_token) {
         if($fire_token != null){
		            $account= User::where('firebase_token',$fire_token)->first();
		            if($account==null)
		                    $account= Driver::where('firebase_token',$fire_token)->first();
		
		                $body =$account->is_android==1 ? 'data':'notification';
		        }else{
		                $body='notification';
		        }
        // return [$mess,$fire_token];
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array (
              'to' => $fire_token,
			  "{$body}" => array (
                                    "body"  => $mess,
                                    "title" => "dokhn",
                                    "icon"  => "myicon",
                                    "sound" => "notify.mp3",
                                )
                 );
        $fields = json_encode ( $fields );
        $headers = array (
        // New auth key from Paul
        // AAAAyafs1No:APA91bGPW6NXCFz13_2Gcs0bM-xJX8E78qLI5fA-JGhxZLBYTDvusz6iWyNpKN1aNmak9bMBwjgxXGispgece8YyYD7TKFRqIVpthRm72P7rzoCIQ9_MvFq_VlAQomNjErz_KeVSe-bF
        
        // AAAANlO2sQE:APA91bESqjXvHhetU-HozSUDkNPx7Bamirwtv6opfceP_OoARAWl05XVOxhBwEjiMsusLWz4H77OKDyVt62dlLUsaUQPHtLEVHQ6HxAoXBT-ff-WHsPYJpC3QhLDHIGAvha4_-id9Myu
        
        'Authorization: key=' . "AAAAyafs1No:APA91bGPW6NXCFz13_2Gcs0bM-xJX8E78qLI5fA-JGhxZLBYTDvusz6iWyNpKN1aNmak9bMBwjgxXGispgece8YyYD7TKFRqIVpthRm72P7rzoCIQ9_MvFq_VlAQomNjErz_KeVSe-bF",
        // 'Authorization: key=' . "AIzaSyDW4TaX7aBLZcEDSd5fJ2z_xp1s4Lo8p-8",
        'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

        $result = curl_exec ( $ch );
        curl_close ( $ch );
        return $result;
    }

    public static function sendFCMEdit($mess_ar,$mess_en,$type,$fire_token) {
        // return [$mess,$fire_token];
         if($fire_token != null){
		            $account= User::where('firebase_token',$fire_token)->first();
		            if($account==null)
		                    $account= Driver::where('firebase_token',$fire_token)->first();
		
		                $body =$account->is_android==1 ? 'data':'notification';
		        }else{
		                $body='notification';
		        }
		        
        if($fire_token!=null){
            $url = 'https://fcm.googleapis.com/fcm/send';
            $fields = array (
                'to' => $fire_token,
                "{$body}" => array (
                    "body_ar"  => $mess_ar,
                    "body_en"  => $mess_en,
                    "type"  => $type,
                    "title" => "dokhn",
                    "icon"  => "myicon",
                    "sound" => "notify.mp3",
                )
            );
            $fields = json_encode ( $fields );
            $headers = array (
            // New auth key from Paul
            // AAAAyafs1No:APA91bGPW6NXCFz13_2Gcs0bM-xJX8E78qLI5fA-JGhxZLBYTDvusz6iWyNpKN1aNmak9bMBwjgxXGispgece8YyYD7TKFRqIVpthRm72P7rzoCIQ9_MvFq_VlAQomNjErz_KeVSe-bF
            
            // AAAANlO2sQE:APA91bESqjXvHhetU-HozSUDkNPx7Bamirwtv6opfceP_OoARAWl05XVOxhBwEjiMsusLWz4H77OKDyVt62dlLUsaUQPHtLEVHQ6HxAoXBT-ff-WHsPYJpC3QhLDHIGAvha4_-id9Myu
            
            'Authorization: key=' . "AAAAyafs1No:APA91bGPW6NXCFz13_2Gcs0bM-xJX8E78qLI5fA-JGhxZLBYTDvusz6iWyNpKN1aNmak9bMBwjgxXGispgece8YyYD7TKFRqIVpthRm72P7rzoCIQ9_MvFq_VlAQomNjErz_KeVSe-bF",
            // 'Authorization: key=' . "AIzaSyDW4TaX7aBLZcEDSd5fJ2z_xp1s4Lo8p-8",
            'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt ( $ch, CURLOPT_URL, $url );
            curl_setopt ( $ch, CURLOPT_POST, true );
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
    
            $result = curl_exec ( $ch );
    
            curl_close ( $ch );
            // dd($result);
            return $result;
    }
        
    }

}