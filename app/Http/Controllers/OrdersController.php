<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\refusalReasons;
use App\Deliveryorder;
use App\Orderreview;
use App\Normalorder;
use App\Specialorder;
use Flashy;
use DB;
use   App\Http\Controllers\NotificationController;

class OrdersController extends Controller
{

 public function index () {
      
        $Order = Order::all();
        
        $DeliveryOrder = Order::where('is_delivery',1)->get();

        $RefusedOrder = refusalReasons::get();
        

        $OrderReview = DB::table('ordersReviews')
        ->join('orders','orders.id','ordersReviews.orders_id')
        ->join('users','users.id','orders.users_id')
        ->select('ordersReviews.*','users.first_name as userFirstName','users.last_name as userLastName', 'users.id as userId')
        ->get();

       $NormalOrder =Normalorder::orderBy('id','DESC')->where('is_promotion',1)->get();
       
       $SpecialOrder = Specialorder::orderBy('id','DESC')->get();
       
       DB::table('specialOrders')
       ->join('sauces','sauces.id','specialOrders.sauces_id')
       ->join('cakes','cakes.id','specialOrders.cakes_id')
       ->join('addons','addons.id','specialOrders.addons_id')
       ->join('orders','orders.id','specialOrders.orders_id')
       ->join('users','users.id','orders.users_id')
       ->select('specialOrders.*','sauces.name_ar as saucesName','cakes.name_ar as cakesName','addons.name_ar as addonsName', 'users.first_name as userFirstName','users.last_name as userLastName', 'users.id as userId')
       ->get();


      return view('orders.index', compact('Order', 'DeliveryOrder', 'RefusedOrder', 'OrderReview', 'NormalOrder', 'SpecialOrder'));
    }

    public function changeOrderStatus ($status, $id){

    $Order = Order::find($id);
    $Order->status = $status;
          
    $handel= $Order->save();
    $msgSuccess = "تم التعديل بنجاح";
    $msgFailure = "عذرا! لم تم التعديل";
    Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
    
    $messages=[];
    $content=[];
    
    $messages['recieved']['ar']='تم استقبال طلبك';
    $messages['recieved']['en']='your order has been recieved';
    
    $messages['waiting']['ar']='طلبك قيد الانتظار';
    $messages['waiting']['en']='Your order is pending';
    
     $messages['refused']['ar']='تم رفض طلبك';
     $messages['refused']['en']='Your order has been declined';
    
     $messages['finished']['ar']='عزيزى العميل تم وصول الطلبية بنجاح للمعرض بنجاح الرجاء استلامها';
     $messages['finished']['en']='Dear Customer The order has arrived successfully at the exhibition successfully. Please receive it';
    
     NotificationController::sendFCMEdit($messages[$status]['ar'],$messages[$status]['en'],"product",$Order->user->firebase_token);

    // Get Redirected
    return back();
    }
     public function show ($id) {
    	$ORDER = Order::find($id);
//     	$apitoken=$ORDER->user->apitoken;
//     	$ch = curl_init();

//       curl_setopt($ch, CURLOPT_URL,"https://dokhn.magdsoft.com/api/orderInfo");
//       curl_setopt($ch, CURLOPT_POST, 1);
//       curl_setopt($ch, CURLOPT_POSTFIELDS,
//                   "orderId=$id&apiToken=$apitoken&lang=ar");
                  
//       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//             $server_output = json_decode(curl_exec($ch));
//             $order=$server_output;
//             curl_close ($ch);
            // dd($order);
    	return view('orders.show', compact('ORDER'));
    }


    public function orderDestroy ($id){
    
      // Delete the Record
      $handel = Order::destroy($id);
      
      // Get the Messages
      $msgSuccess = "تم حذف الطلب بنجاح";
      $msgFailure = "عذرا! لم يتم حذف الطلب";
      Flashy::success($handel == true ? $msgSuccess : $msgFailure);

      // Get Redirected
      return back();
    }
    
    public function printView ($id) {
      $Order = Order::find($id);
      return View('orders.orderPrint', compact('Order'));
    }
}