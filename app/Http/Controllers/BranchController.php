<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;
use App\City;
use App\Town;
use Flashy;
use Validator;
use Exception;

class BranchController extends Controller
{
    //
    private $rules = [

    ];
    private $messages = [

    ];

    public function index () {
    	$Branch = Branch::all();
    	return view('branches.branchIndex', compact('Branch'));
    }

    public function branchAdd () {
    	return view('branches.branchAdd');
    }

    public function branchCreate (Request $request) {

        $rules = [
            'name_ar' =>'required|min:3|max:40',
            'name_en' =>'required|min:3|max:40',
            'latitude' =>'required',
            'logitude' =>'required',
            'address_ar' =>'required|min:3|max:40',
            'address_en' =>'required|min:3|max:40'
        ];
       $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل',
            'min'  =>' لا يمكن ان يقل  الاسم عن 3 حروف'  ,
            'max'  =>' لا يمكن ان يزيد  الاسم عن 40 حروف'  ,
        ];

        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }

        // Create New Dessert Record
        $Branch = new Branch;
        $Branch->name_ar = $request->name_ar;
        $Branch->name_en = $request->name_en;
        $Branch->latitude = $request->latitude;
        $Branch->logitude = $request->logitude;
        $Branch->address_ar = $request->address_ar;
        $Branch->address_en = $request->address_en;
        $handel = $Branch->save();

		// Get the Messages
        $msgSuccess = "تمت اضافة الفرع بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة الفرع";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
		
        // redirected for the Index page
		return redirect('/branches');
    }

    public function branchEdit ($id) {
        $Branch = Branch::find($id);
        return view('branches.branchEdit', compact('Branch'));
    }

    public function branchUpdate(Request $request){
       
        $rules = [
            'name_ar' =>'required|min:3|max:40',
            'name_en' =>'required|min:3|max:40',
            'latitude' =>'required',
            'logitude' =>'required',
            'address_ar' =>'required|min:3|max:40',
            'address_en' =>'required|min:3|max:40'
        ];
       $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل',
            'min'  =>' لا يمكن ان يقل  الاسم عن 3 حروف'  ,
            'max'  =>' لا يمكن ان يزيد  الاسم عن 40 حروف'  ,
        ];
        
        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $Branch = Branch::find($request->id);
        $Branch->name_ar = $request->name_ar;
        $Branch->name_en = $request->name_en;
        $Branch->latitude = $request->latitude;
        $Branch->logitude = $request->logitude;
        $Branch->address_ar = $request->address_ar;
        $Branch->address_en = $request->address_en;
        $handel = $Branch->save();
        
        // Get the Messages
        $msgSuccess = "تم تعديل الفرع بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل الفرع";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        
        // redirected for the Index page
		return redirect('/branches');
    }


    public function branchDestroy ($id) {
    	// Delete the Record
        $Branch = Branch::destroy($id);
        
        // Get the Messages
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل";
        Flashy::success($Branch ? $msgSuccess : $msgFailure);
        
        // redirected for the Index page
        return redirect('/branches');
    }

    // Cities Operation
    public function indexCities () {
    	$City = City::all();
    	return view('branches.cityIndex', compact('City'));
    }

    public function cityAdd () {
    	return view('branches.cityAdd');
    }

    public function cityCreate (Request $request) {

        $rules = [
            'name_ar' =>'required|min:3|max:40',
            'name_en' =>'required|min:3|max:40'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل',
            'min'  =>' لا يمكن ان يقل  الاسم عن 3 حروف'  ,
            'max'  =>' لا يمكن ان يزيد  الاسم عن 40 حروف'  ,
        ];

        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) { 
            
                return redirect()->back()->withErrors($errors)->withInput($request->all());
        }

        // Create New Dessert Record
        $City = new City;
        $City->name_ar = $request->name_ar;
        $City->name_en = $request->name_en;
        $handel = $City->save();


		 // Get the Messages
        $msgSuccess = "تمت اضافة المدينة بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة المدينة";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);

        // redirected for the Index page
        return redirect('/branches/cities');
    }

    public function cityEdit ($id) {
        $City = City::find($id);
        return view('branches.cityEdit', compact('City'));
    }

    public function cityUpdate(Request $request){
        $rules = [
            'name_ar' =>'required|min:3|max:40',
            'name_en' =>'required|min:3|max:40'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل',
            'min'  =>' لا يمكن ان يقل  الاسم عن 3 حروف'  ,
            'max'  =>' لا يمكن ان يزيد  الاسم عن 40 حروف'  ,
        ];

        
        //Validate
        $errors = Validator::make($request->all(),$rules,$messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $City = City::find($request->id);
        $City->name_ar = $request->name_ar;
        $City->name_en = $request->name_en;
        $handel = $City->save();
        
         // Get the Messages
        $msgSuccess = "تم تعديل المدينة بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل المدينة";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        
        // redirected for the Index page
        return redirect('/branches/cities');
    }


    public function cityDestroy ($id) {
    	$City = false;
        // Try the Deletion
        try {
             $City = City::destroy($id);
        } catch (Exception $e) {
            $msgFailure = "لا يمكن الحذف  ... توجد بيانات مرتبطة بهذا الحقل.";
            Flashy::success($msgFailure);
            return redirect()->route('cities');
        }
       	// Get the Messages
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل ";
        Flashy::success($City == true ? $msgSuccess : $msgFailure);
        
        // redirected for the Index page
        return redirect('/branches/cities');
    }

    // Towns Operation
    public function listCityTowns ($id) {
    	// list towns inside the city
	    $Town = Town::where('cities_id', '=', $id)->get();
    	return view('branches.cityTownIndex', compact('Town'));
    }

    public function townAdd () {
    	// Get The list of cities
        $City = City::pluck('name_ar', 'cityId');
    	return view('branches.townAdd', ['City'=>$City]);
    }

    public function townCreate (Request $request) {
		$rules = [
		    
            'name_ar' =>'required|min:3|max:40',
            'name_en' =>'required|min:3|max:40',
     	   'delivery_price'=>'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'cities_id'=>'required|integer',
        ];
        
        $messages = [
        	'required'  =>'لا بد من ادخال هذا الحقل' , 
        	'min'  =>' لا يمكن ان يقل  الاسم عن 3 حروف'  ,
            'max'  =>' لا يمكن ان يزيد  الاسم عن 40 حروف'  , 
            'regex'  =>' يجب ادخال السعر بشكل صحيح '  ,

        ];
       
        
        //Validate
      $errors = Validator::make($request->all(), $rules, $messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }

		//Create new record
		$town=new Town();
        $town->name_ar=$request->name_ar;
        $town->name_en=$request->name_en;
        $town->latitude=$request->latitude;
        $town->logitude=$request->logitude;
        $town->delivery_price=$request->delivery_price;
        $town->cities_id=$request->cities_id;
        $handel =  $town->save();
		
        // Get the city Inserted id
		$id = $town->cities_id;

		// Get the Messages
        $msgSuccess = "تمت اضافة المدينة بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة المدينة";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        
        // redirected for the City Index page
		return redirect("/branches/listCityTowns/$id");
    }

    public function townEdit ($id) {
        $Town = Town::find($id);
        $City = City::pluck('name_ar', 'cityId');
        return view('branches.townEdit',['Town'=>$Town, 'City'=>$City]);
    }

    public function townUpdate(Request $request){
    
    	$rules = [
	        'name_ar' =>'required|min:3|max:40',
            'name_en' =>'required|min:3|max:40',
            'latitude'=>'required|min:3',
            'logitude'=>'required|min:3',
     	  'delivery_price'=>'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'cities_id'=>'required|integer'
        ];
        
        $messages = [
        	'required'  =>'لا بد من ادخال هذا الحقل',
    		'min'  =>' لا يمكن ان يقل  الاسم عن 3 حروف'  ,
            'max'  =>' لا يمكن ان يزيد  الاسم عن 40 حروف'  ,
            'regex'  =>' يجب ادخال السعر بشكل صحيح '  ,

        ];
        
        
        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
        
       	//Update record
		$town = Town::find($request->id);
        $town->name_ar=$request->name_ar;
        $town->name_en=$request->name_en;
        $town->latitude=$request->latitude;
        $town->logitude=$request->logitude;
        $town->delivery_price=$request->delivery_price;
        $town->cities_id=$request->cities_id;
        $handel =  $town->save();
   		
        // Get the city Inserted id
		$id = $town->cities_id;
        
        // Get the Messages
        $msgSuccess = "تم تعديل المدينة بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل المدينة";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        
        // redirected for the City Index page
		return redirect("/branches/listCityTowns/$id");
    }


    public function townDestroy ($id) {
		// initialize the variable
    	$town = false;
        $cityTown = Town::find($id);
        $cityId = $cityTown->cities_id;
        try {
        	//try to destroy the record
             $town = Town::destroy($id);
        } catch (Exception $e) {
            $msgFailure = "لا يمكن الحذف  ... توجد بيانات مرتبطة بهذا الحقل.";
            Flashy::success($msgFailure);
		return redirect("/branches/listCityTowns/$cityId");
        }
       	// Get the Messages
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل ";
        Flashy::success($town == true ? $msgSuccess : $msgFailure);
        
        // redirected for the City Index page
		return redirect("/branches/listCityTowns/$cityId");
    }

}
