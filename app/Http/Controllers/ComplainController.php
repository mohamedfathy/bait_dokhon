<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Complain;
use App\User;
use App\Driver;
use Flashy;


class ComplainController extends Controller
{
    //

    public function index () {
    	$Complain = Complain::orderBy('id', 'DESC')->get();
    	return view('complain.index', compact('Complain'));
    }
    
    public function complainDestroy ($id){
    
      // Delete the Record
	  $handel = Complain::destroy($id);
      
      // Get the Messages
      $msgSuccess = "تم حذف الشكوى بنجاح";
      $msgFailure = "عذرا! لم يتم حذف الشكوى";
      Flashy::success($handel == true ? $msgSuccess : $msgFailure);

      // Get Redirected
      return back();
    }
    
    public function Search (Request $request){
    
        if($request->has('q')){
        $User =  User::where('first_name', 'LIKE', '%'.$request->q.'%')
        ->orWhere('last_name', 'LIKE', '%'.$request->q.'%')
        ->get();
        
        $Driver =  Driver::where('first_name', 'LIKE', '%'.$request->q.'%')
        ->orWhere('last_name', 'LIKE', '%'.$request->q.'%')
        ->get();
        
        $Complain = Complain::where('timestamp', 'LIKE', '%'.$request->q.'%')
        ->get();
        }
        return view('users.search', compact('User', 'Driver', 'Complain')); 
    }
    

}
