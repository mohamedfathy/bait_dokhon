<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
use App\Editrequest;
use Flashy;
use Validator;
use DB;

class DriversController extends Controller
{

      public function index () {
         $Driver = Driver::where('is_accepted', '1')->get();
         $DriverApplicant = Driver::all()->where('is_accepted', '0');
         $DriverEdit = Editrequest::all();
         return view('drivers.index', compact('Driver', 'DriverApplicant', 'DriverEdit'));
      }
      
      public function show ($id) {
         $Driver = Driver::find($id);
         return view('drivers.show', compact('Driver'));
      }

   	  public function search (Request $request) {
   		if ($request->has('q')){
   		$Driver = Driver::where('first_name', 'LIKE', '%'.$request->q.'%')
   		->orWhere('last_name', 'LIKE', '%'.$request->q.'%')
   		->orWhere('phone', 'LIKE', '%'.$request->q.'%')
        ->get();
   		}
   		return view ('drivers.index', compact('Driver'));
   	  }
 
    
      public function activation ($id){
         $Driver = Driver::find($id);
         if($Driver->is_active == 1){
           $Driver->is_active = 0;
         }
         else{$Driver->is_active = 1;}
         $Driver->save();
         $msgSuccess = " تم تفعيل السائق بنجاح ";
         $msgFailure = " تم تعطيل السائق بنجاح ";
         Flashy::success($Driver->is_active == 1 ? $msgSuccess : $msgFailure);
        return redirect('/drivers');
      }
      
      
      public function apply ($id){
         $Driver = Driver::find($id);
         if($Driver->is_accepted == 1){
           $Driver->is_accepted = 0;
         }
         else{$Driver->is_accepted = 1;}
         $Driver->save();
         $msgSuccess = " تم قبول السائق بنجاح ";
         $msgFailure = " تم رفض السائق بنجاح ";
         Flashy::success($Driver->is_accepted == 1 ? $msgSuccess : $msgFailure);
         return redirect('/drivers/indexApplicants'); 
      }
      

	  public function deleteApplicant ($id, $adminMessage) {
     
     	// Get the Driver Object
      	$Driver = Driver::find($id);
      	
        // Get The Email Property
        $email = $Driver->email;
        $view = 'email.send';
        $content = ['adminMessage'=>$adminMessage];
        $subject="بيت الدخن";
        
       	if ( $email != null ) {
  
       		$this->email($view, $email, $content, $subject);

        	// Delete the Record
        	$handel = Driver::destroy($id);
        
            // Get the Messages
            $msgSuccess = "تم حذف طلب الانضمام بنجاح";
            $msgFailure = "عذرا! لم يتم حذف طلب الانضمام";
            Flashy::success($handel == true ? $msgSuccess : $msgFailure);
		}
        // Get Redirected
        return back();
      }
      
      public function indexApplicants () {
         $Driver = Driver::where('is_accepted', '0')->paginate(20);
         return view('drivers.applicant', compact('Driver'));
      }

      public function editRequest () {
         $DriverEdit = Editrequest::all();
         return view('drivers.edits', compact('DriverEdit'));
      }
      
      public function acceptEditRequest ($id, $key) {
      
      	 // Get the Object
		 $Driver = Driver::find($id);
         
         if($key== 'car_photo1' ||  $key== 'car_photo2' ||  $key== 'car_photo3' ||  $key== 'car_photo4'  ){
            // Assign the value to the key
           
            ($Driver->Carphoto[substr($key,-1) -1])->url=EditRequest::where('drivers_id',$Driver->id)->where('field',$key)->first()->content;
            ($Driver->Carphoto[substr($key,-1) -1])->save();
            
            
         }else{
            
            $Driver->$key = EditRequest::where('drivers_id',$Driver->id)->where('field',$key)->first()->content;
            
         }
         // Save the Edits
         $Driver->save();
         
         // Drop the record From Editrequest Table
         $handel = Editrequest::where('field', '=', $key)->delete();
         
                 // Get the messages
        $msgSuccess = "تم تعديل بيانات السائق بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل بيانات السائق";
        Flashy::success($handel == true ? $msgSuccess : $msgFailure);
         
         // Get Redirected 
         return redirect('/drivers/editRequest'); 
      }
      
      public function refuseEditRequest ($id, $key, $value) {
         // Drop the record From Editrequest Table
// return ( [$id,$key, (int)$value]);
         Editrequest::where('drivers_id',$id)->where('field', $key)->where('content',$value)->delete();
         
         // Get Redirected 
         return redirect('/drivers/editRequest'); 
      }
      


}
