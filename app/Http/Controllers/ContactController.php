<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Appsetting;
use Flashy;
use Validator;

class ContactController extends Controller
{
    //
	
    private $rules = [

    ];
    private $messages = [

    ];

    public function contactIndex () {
        $Contact = Contact::all();
        $Appsetting = Appsetting::find(1);
        return view('contact.index', compact('Contact', 'Appsetting'));
    }

    public function emailEdit ($id) {
        $Appsetting = Appsetting::find($id);
        return view('contact.emailEdit', compact('Appsetting'));
    }

    public function emailUpdate(Request $request){
        $rules = [
    		'contact_email'=>'required||regex:/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/|min:5|max:30',
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل',
            'regex'  =>'يجب ادخال الايميل بشكل صحيح'
            ];
        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $Appsetting = Appsetting::find($request->id);
        $Appsetting->contact_email = $request->contact_email;
        $handel = $Appsetting->save();
        $msgSuccess = "تم تعديل الايميل بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل الايميل";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        // Data to Return
        $Contact = Contact::all();
        $Appsetting = Appsetting::find(1);
        return view('contact.index', compact('Appsetting', 'Contact'));
    }

    public function phoneAdd() {
        return view('contact.phoneAdd');
    }

    public function phoneCreate(Request $request){
        $rules = [
            'phone' =>'min:8|max:15|required|unique:contactPhones,phone'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];

        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) {
            return redirect()->back()->withErrors($errors);
        }

        // Create Record
        $Contact = new Contact;
        $Contact->phone = $request->phone;
        $handel = $Contact->save();

        $msgSuccess = "تمت اضافة التليفون بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة التليفون";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);

        // Data to Return
        $Contact = Contact::all();
        $Appsetting = Appsetting::find(1);
        return view('contact.index', compact('Appsetting', 'Contact'));
    }

    public function phoneDestroy ($id) {
        $Contact = Contact::destroy($id);
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل";
        Flashy::success($Contact == true ? $msgSuccess : $msgFailure);
        return redirect('/contact');
    }

    public function aboutIndex() {
        $Appsetting = Appsetting::find(1);
        return view('about.index', compact('Appsetting'));
    }

    public function aboutEdit ($id) {
        $Appsetting = Appsetting::find($id);
        return view('about.edit', compact('Appsetting'));
    }

    public function aboutUpdate(Request $request){
        $rules = [
            'about_us_ar' =>'required',
            'about_us_en' =>'required'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];
        //Validate
        $errors = Validator::make($request->all(), $this->rules, $this->messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $Appsetting = Appsetting::find($request->id);
        $Appsetting->about_us_ar = $request->about_us_ar;
        $Appsetting->about_us_en = $request->about_us_en;
        $handel = $Appsetting->save();
        $msgSuccess = "تم تعديل عن الموقع بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل عن الموقع";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        // Data to Return
        $Appsetting = Appsetting::find(1);
        return view('about.index', compact('Appsetting'));
    }


      public function feesIndex() {
        $Appsetting = Appsetting::find(1);
        return view('fees.index', compact('Appsetting'));
    }

    public function feesEdit ($id) {
        $Appsetting = Appsetting::find($id);
        return view('fees.edit', compact('Appsetting'));
    }

    public function feesUpdate(Request $request){
        $rules = [
			'fees'=>'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل',
            'regex'  =>' يجب ادخال رقم بشكل صحيح'
        ];
        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $Appsetting = Appsetting::find($request->id);
        $Appsetting->fees = $request->fees;
        $handel = $Appsetting->save();
        $msgSuccess = "تم تعديل بنجاح";
        $msgFailure = "عذرا! لم يتم ";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        // Data to Return
        $Appsetting = Appsetting::find(1);
        return view('fees.index', compact('Appsetting'));
    }

}
