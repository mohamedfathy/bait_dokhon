<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Flashy;

use App\discount_codes;
class discount_codesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $discount_codes=discount_codes::all();
        return view('discount_codes.index',compact('discount_codes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('discount_codes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            
            "discount"=>"required|digits_between:0,99",
            "end_date"=>"required|date",
            "count_uses"=>"required|digits_between:0,999999999999999"
            
        ];
        
        $messages=[
            
            "required"=>"يجب ادخال قيمة ",
            "digits_between"=>"يجب ادخال الرقم بشكل صحيح",
            "date"=>"يجب دخال التاريخ بشكل صحيح"
        ] ;   
        
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
        $discount_codes= new discount_codes();
        
        $discount_codes->discount=$request->discount;
        $discount_codes->end_date=$request->end_date;
        $discount_codes->count_uses=$request->count_uses;
        
        do {
            $random= rand(pow(10, 4), pow(10, 5)-1);
        }while(discount_codes::where('code',$random)->count() != 0);
        
        $discount_codes->code=$random;
        $discount_codes->created_at=date("Y-m-d");
        $discount_codes->save();
        
        Flashy::success('تمت الاضافة بنجاح');
        return redirect('/discount_codes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $discount_code=  discount_codes::find($id);
        return view('discount_codes.edit',compact('discount_code'));
        
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules=[
            
            "discount"=>"required|digits_between:0,99",
            "end_date"=>"required|date",
            "count_uses"=>"required|digits_between:0,999999999999999"
            
        ];
        
        $messages=[
            
            "required"=>"يجب ادخال قيمة ",
            "digits_between"=>"يجب ادخال الرقم بشكل صحيح",
            "date"=>"يجب دخال التاريخ بشكل صحيح"
        ] ;   
        
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
        $discount_codes=  discount_codes::find($request->id);
        
        $discount_codes->discount=$request->discount;
        $discount_codes->end_date=$request->end_date;
        $discount_codes->count_uses=$request->count_uses;
        
        // do {
        //     $random=rand(10000, 99999);
        // }while(discount_codes::where('code',$random)->count() == 0);
        
        // $discount_codes->code=$random;
        $discount_codes->save();
        Flashy::success('تم التحديث بنجاح');

        
        return redirect('/discount_codes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function activation($id)
    {
    $discount_codes=    discount_codes::findOrFail($id);
 
    $discount_codes->is_active=  $discount_codes->is_active == 0 ? 1:0;
    $discount_codes->save();
    Flashy::success('تم تغيير الحالة  بنجاح');
    return back();
    
    
    }
    
    public function destroy($id)
    {
            Flashy::success('تم المسح  بنجاح');

        discount_codes::Destroy($id);
        
        return back();
    }
}
