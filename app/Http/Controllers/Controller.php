<?php
https://files.apis.magdsoft.com/files/dokhn-admin/app/
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Mail;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function email($view,$email,$content,$subject)
   	{
        $send = Mail::send($view,$content,function ($message) use ($subject,$email) {
        $message->from('dokhan@magdsoft.com', 'Bait Eldokhon');
        $message->subject($subject);
        $message->to($email);
    });

   }
}
