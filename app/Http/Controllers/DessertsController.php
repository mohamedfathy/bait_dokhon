<?php

namespace App\Http\Controllers;
use   App\Http\Controllers\NotificationController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Dessert;
use App\Dessertsize;
use App\Addon;
use App\Cake;
use App\Sauce;
use Flashy;
use Validator;
use Exception;
use App\Dessertpromotion;


class DessertsController extends Controller
{

    private $rules = [

    ];
    private $messages = [

    ];


    public function index () {
    	$Dessert = Dessert::paginate(20);
    	return view('desserts.dessertIndex', compact('Dessert'));
    }
    
    public function dessertAdd () {
    	return view('desserts.dessertAdd');
    }
    
    public function dessertCreate (Request $request) {
    
    	// Make the vaildation 
        $rules = [
            
            'name_ar' =>'required|min:3|max:40',
            'name_en' =>'required|min:3|max:40',
            'description_ar'=>'required|min:3|max:40',
            'description_en'=>'required|min:3|max:40',
       	    'price'=>'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'cock_time'=>'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
         'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',	
    
        ];
          $messages = [

            'required'=>'لا بد من ادخال هذا الحقل',
            'min'     =>' لا يمكن ان يقل  الاسم عن 3 حروف'  ,
            'max'     =>' لا يمكن ان يزيد  الاسم عن 40 حروف'  ,
           'image'  =>' يجب ادخال صورة حقيقية '  ,
            'mimes'  =>' يجب ادخال صورة حقيقية '  ,
            'max'  =>'حجم الصورة اكبر من 1 ميجا  '  ,
            'regex'   =>' يجب ادخال السعر بشكل صحيح '  ,

        ];

        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
		
        
        // Save the image
        $file=$request->image;
        $ad_photo = str_random(30) . '.' . $file->getClientOriginalExtension();
        $file->move( public_path('uploades/DessertSizes') , $ad_photo);
        $url=Request()->root().'/uploades/DessertSizes/'.$ad_photo;

        // Create New Dessert Record
        $Dessert = new Dessert;
        $Dessert->name_ar = $request->name_ar;
        $Dessert->name_en = $request->name_en;
        $handel = $Dessert->save();

        // Create New Dessert Size Record
        $Dessertsize = new Dessertsize;
        $Dessertsize->desserts_id =$Dessert->id;
        $Dessertsize->description_ar = $request->description_ar;
        $Dessertsize->description_en = $request->description_en;
        $Dessertsize->image = $url;
        $Dessertsize->price = $request->price;
        $Dessertsize->cock_time = $request->cock_time;
        $handel = $Dessertsize->save();

        $Dessert->dessertSizes_id = $Dessertsize->id;
        $handel=$Dessert->save();
        
        // Get the messages
        $msgSuccess = "تمت اضافة الحلوى بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة الحلوى";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);

        // Get Redirected
        return redirect('/desserts');
    }
    
    
    public function dessertEdit ($id) {
        $Dessert = Dessert::find($id);
        return view('desserts.dessertEdit', compact('Dessert'));
    }

    public function dessertUpdate(Request $request){    
     $rules = [
            
            'name_ar' =>'required|min:3|max:40',
            'name_en' =>'required|min:3|max:40',
    
        ];
          $messages = [

            'required'=>'لا بد من ادخال هذا الحقل',
            'min'     =>' لا يمكن ان يقل  الاسم عن 3 حروف'  ,
            'max'     =>' لا يمكن ان يزيد  الاسم عن 40 حروف'  ,

        ];
        
        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $Dessert = Dessert::find($request->id);
        $Dessert->name_ar = $request->name_ar;
        $Dessert->name_en = $request->name_en;
        $handel = $Dessert->save();
        // Get The Messages
        $msgSuccess = "تم تعديل الحلوى بنجاح";
        $msgFailure = "عذرا! لم يتم التعديل";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        
        // Get Redirected
        return redirect('/desserts');
    }


    public function dessertDestroy ($id) {
    	// initialize the variable
        $Dessert = false;
        try {
             $Dessert = Dessert::destroy($id);
        } catch (Exception $e) {
            $msgFailure = "لا يمكن الحذف  ... توجد بيانات مرتبطة بهذا الحقل.";
            Flashy::success($msgFailure);
            return redirect()->route('desserts');
        }
        // Get The Messages
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل ";
        Flashy::success($Dessert == true ? $msgSuccess : $msgFailure);
        
        // Get Redirected
        return redirect('/desserts');
    }
    
    
	// Index all Extra
    public function dessertExtra () {
    	$Addon = Addon::all();
		$Cake = Cake::all();
    	$Sauce = Sauce::all();
    	return view('desserts.extraIndex', compact('Addon', 'Cake', 'Sauce'));
    }


    //Addon Operation
    public function addonAdd () {
        return view('desserts.extraAdd');
    }

    public function addonCreate(Request $request){
        $rules = [
            
            'name_ar' =>'required|min:3|max:40',
            'name_en' =>'required|min:3|max:40',
             'price'=>'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'cock_time'=>'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        
        ];
        
         $messages = [

            'required'=>'لا بد من ادخال هذا الحقل',
            'min'     =>' لا يمكن ان يقل  الاسم عن 3 حروف'  ,
            'max'     =>' لا يمكن ان يزيد  الاسم عن 40 حروف'  ,
            'regex'   =>' يجب ادخال السعر بشكل صحيح '  ,

        ];

        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
          $messages = [

            'required'=>'لا بد من ادخال هذا الحقل',
            'min'     =>' لا يمكن ان يقل  الاسم عن 3 حروف'  ,
            'max'     =>' لا يمكن ان يزيد  الاسم عن 40 حروف'  ,
            'image'   =>' لا بد من ادخال صورة حقيقية '  , 
            'regex'   =>' يجب ادخال السعر بشكل صحيح '  ,

        ];

		
        // Create Record
        $Addon = new Addon;
        $Addon->name_ar = $request->name_ar;
        $Addon->name_en = $request->name_en;
        $Addon->price = $request->price;
        $Addon->cock_time = $request->cock_time;
        $handel = $Addon->save();
		
        // Get The Messages
        $msgSuccess = "تمت اضافة الاضافة بنجاح ";
        $msgFailure = "عذرا! لم يتم اضافة الاضافة";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        
        // Get Redirected
        return redirect('/desserts/dessertExtra');  
    }

    public function addonEdit ($id) {
        $Extra = Addon::find($id);
        return view('desserts.extraEdit', compact('Extra'));
    }

    public function addonUpdate(Request $request){
        $rules = [
            
            'name_ar' =>'required|min:3|max:40',
            'name_en' =>'required|min:3|max:40',
             'price'=>'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'cock_time'=>'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        
        ];
        
         $messages = [

            'required'=>'لا بد من ادخال هذا الحقل',
            'min'     =>' لا يمكن ان يقل  الاسم عن 3 حروف'  ,
            'max'     =>' لا يمكن ان يزيد  الاسم عن 40 حروف'  ,
            'regex'   =>' يجب ادخال السعر بشكل صحيح '  ,

        ];
        //Validate
        
        $errors = Validator::make($request->all(), $rules, $messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $Addon = Addon::find($request->id);
        $Addon->name_ar = $request->name_ar;
        $Addon->name_en = $request->name_en;
        $Addon->price = $request->price;
        $Addon->cock_time = $request->cock_time;
        $handel = $Addon->save();
		
        // Get The Messages
        $msgSuccess = "تم تعديل الاضافة بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل الاضافة";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        
        // Get Redirected
        return redirect('/desserts/dessertExtra'); 
    } 

    public function addonDestroy ($id) {
        $Extra = false;
        try {
             $Extra = Addon::destroy($id);
        } catch (Exception $e) {
            $msgFailure = "لا يمكن الحذف  ... توجد بيانات مرتبطة بهذا الحقل.";
            Flashy::success($msgFailure);
            return redirect()->route('dessertExtra');
        }
       
       	// Get The Messages
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل ";
        Flashy::success($Extra == true ? $msgSuccess : $msgFailure);
        
        // Get Redirected
        return redirect('/desserts/dessertExtra'); 
    }


    //cake Operation
    public function cakeAdd () {
        return view('desserts.extraAdd');
    }

    public function cakeCreate(Request $request){
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required',
            'price' =>'required',
            'cock_time' =>'required'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];

        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }

        // Create Record
        $Cake = new Cake;
        $Cake->name_ar = $request->name_ar;
        $Cake->name_en = $request->name_en;
        $Cake->price = $request->price;
        $Cake->cock_time = $request->cock_time;
        $handel = $Cake->save();
        
		// Get the Messages
        $msgSuccess = "تمت اضافة الكيكة بنجاح ";
        $msgFailure = "عذرا! لم يتم اضافة الكيكة";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        
        
        // Get Redirected
        return redirect('/desserts/dessertExtra'); 
    }

    public function cakeEdit ($id) {
        $Extra = Cake::find($id);
        return view('desserts.extraEdit', compact('Extra'));
    }
    
    public function cakeUpdate(Request $request){
    
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required',
            'price' =>'required',
            'cock_time' =>'required'
        ];
        
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];
        
        //Validate
        $errors = Validator::make($request->all(), $this->rules, $this->messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $Cake = Cake::find($request->id);
        $Cake->name_ar = $request->name_ar;
        $Cake->name_en = $request->name_en;
        $Cake->price = $request->price;
        $Cake->cock_time = $request->cock_time;
        $handel = $Cake->save();
        
        // Get The Messages
        $msgSuccess = "تم تعديل الاضافة بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل الاضافة";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        
        // Get Redirected
        return redirect('/desserts/dessertExtra'); 
    } 

	
    public function cakeDestroy ($id) {
        $Extra = false;
        try {
             $Extra = Cake::destroy($id);
        } catch (Exception $e) {
            $msgFailure = "لا يمكن الحذف  ... توجد بيانات مرتبطة بهذا الحقل.";
            Flashy::success($msgFailure);
            return redirect()->route('dessertExtra');
        }
       	
        // Get The Messages
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل ";
        Flashy::success($Extra == true ? $msgSuccess : $msgFailure);
        
        // Get Redirected
        return redirect('/desserts/dessertExtra'); 
    }

    //sauce Operation
    public function sauceAdd () {
        return view('desserts.extraAdd');
    }

    public function sauceCreate(Request $request){
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required',
            'price' =>'required',
            'cock_time' =>'required'
        ];
        
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];

        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }

        // Create Record
        $Sauce = new Sauce;
        $Sauce->name_ar = $request->name_ar;
        $Sauce->name_en = $request->name_en;
        $Sauce->price = $request->price;
        $Sauce->cock_time = $request->cock_time;
        $handel = $Sauce->save();
		
        // Get The Messages
        $msgSuccess = "تمت اضافة الصوص بنجاح ";
        $msgFailure = "عذرا! لم يتم اضافة الصوص";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        
        // Get Redirected
        return redirect('/desserts/dessertExtra'); 
    }

    public function sauceEdit ($id) {
        $Extra = Sauce::find($id);
        return view('desserts.extraEdit', compact('Extra'));
    }
    
    public function sauceUpdate(Request $request){
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required',
            'price' =>'required',
            'cock_time' =>'required'
        ];
        
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];
        
        //Validate
        $errors = Validator::make($request->all(), $this->rules, $this->messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $Sauce = Sauce::find($request->id);
        $Sauce->name_ar = $request->name_ar;
        $Sauce->name_en = $request->name_en;
        $Sauce->price = $request->price;
        $Sauce->cock_time = $request->cock_time;
        $handel = $Sauce->save();
        
        // Get The Messages
        $msgSuccess = "تم تعديل الصوص بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل الصوص";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        
        // Get Redirected
        return redirect('/desserts/dessertExtra'); 
    }

    public function sauceDestroy ($id) {
        $Extra = false;
        try {
             $Extra = Sauce::destroy($id);
        } catch (Exception $e) {
            $msgFailure = "لا يمكن الحذف  ... توجد بيانات مرتبطة بهذا الحقل.";
            Flashy::success($msgFailure);
            return redirect()->route('dessertExtra');
        }
       	// Get The Messages
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل ";
        Flashy::success($Extra == true ? $msgSuccess : $msgFailure);
        
        // Get Redirected
        return redirect('/desserts/dessertExtra'); 
    } 
      
    public function dessertShow ($id) {
    	$desert = Dessert::find($id);
		$desertSizes = DB::select('SELECT * from dessertSizes where dessertSizes.desserts_id=?',[$id]);
        $desert = Dessert::find($id);
        $Ids =  Dessertpromotion::pluck('dessertSizes_id');
        $DessertpromotionIds = json_decode(json_encode($Ids),TRUE);   
        //dd($DessertpromotionIds);
    	return view('desserts.dessertShow', compact('desert', 'desertSizes', 'DessertpromotionIds'));
    }
    
    public function dessertAddSize($id) {
    	$Dessert = Dessert::find($id);
    	return view('desserts.dessertAddSize', compact('Dessert'));
    }
    
    public function dessertCreateSize (Request $request, $id){
        //Validate the Inputs
    
			$rules = [
		    
	        'description_ar'=>'required|min:3|max:40',
	        'description_en'=>'required|min:3|max:40',
			'price'=>'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
			'cock_time'=>'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/', 
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',	
            ];
        
      $messages = [
            'min'  =>' يجب ادخال ان لا يقل عن 3 حروف '  ,
            'max'  =>' يجب ان لا يزيد عن 40 حرف '  ,
            'regex'  =>' يجب ادخال الرقم بشكل صحيح '  ,
            'numeric'  =>' يجب ادخال الرقم بشكل صحيح '  ,
            'required'  =>' يجب ادخال قيمة '  ,
            'image'  =>' يجب ادخال صورة حقيقية '  ,
            'mimes'  =>' يجب ادخال صورة حقيقية '  ,
            'max'  =>'حجم الصورة اكبر من 1 ميجا  '  ,

        ];
        
          //Validate
      $errors = Validator::make($request->all(), $rules, $messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
        
        // Save the Image
        $file=$request->image;
		$ad_photo = str_random(30) . '.' . $file->getClientOriginalExtension();
		$file->move( public_path('uploades/DessertSizes') , $ad_photo);
		$url=Request()->root().'/uploades/DessertSizes/'.$ad_photo;	    
        
        // Create Record
        $Dessertsize=new Dessertsize();
        $Dessertsize->description_ar = $request->description_ar;
        $Dessertsize->description_en = $request->description_en;
        $Dessertsize->image = $url;
        $Dessertsize->price = $request->price;
        $Dessertsize->cock_time = $request->cock_time;
        $Dessertsize->desserts_id = $id;
		$handel=$Dessertsize->save();
        
        // Get The Messages 
        $msgSuccess = "تمت اضافة حجم الحلوى بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة حجم الحلوى";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);


       
        // Get Redirected
        return redirect('/desserts/dessertShow/'.$id);    
    }
    
    
    public function dessertSizeDefault ( $dessertId, $dessertSizeId){
    	// Get the Dessert Object
    	$desert=Dessert::find($dessertId);
        // Get the Dessert Object
        $desert->dessertSizes_id = $dessertSizeId;
        $handel = $desert->save();
        
        // Get The Messages
        $msgSuccess = "تم تعيين القيمة اﻻفتراضية للحلوي";
        $msgFailure = "عذرا! لم يتم تعيين القيمة اﻻفتراضية الحلوى";
        Flashy::success($handel == true ? $msgSuccess : $msgFailure);

        // Get Redirected
        return redirect('/desserts/dessertShow/'.$dessertId);  
    }
    
    
    public function dessertPromotionAdd ($dessertId, $dessertSizeId ){
  		return view('desserts.dessertPromotionAdd', compact('dessertId', 'dessertSizeId'));
    }
    
    public function dessertPromotionCreate ( Request $request, $dessertId, $dessertSizeId ){

        // Create Record
// dd($Dessertpromotion);
	$rules = [
		    
     	   'discount_percentage'=>'required|numeric|min:1|max:99|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ];
        
        $messages = [
            'min'  =>' يجب ادخال الخصم بشكل صحيح '  ,
            'max'  =>' يجب ادخال الخصم بشكل صحيح '  ,
            'regex'  =>' يجب ادخال الخصم بشكل صحيح '  ,
            'numeric'  =>' يجب ادخال الخصم بشكل صحيح '  ,

        ];
       
        
        //Validate
      $errors = Validator::make($request->all(), $rules, $messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
      Dessertpromotion::where('dessertSizes_id',$dessertSizeId)->delete();
       	$Dessertpromotion = new Dessertpromotion();
        $Dessertpromotion->dessertSizes_id = $dessertSizeId;
        $Dessertpromotion->discount_percentage = $request->discount_percentage;
		$handel = $Dessertpromotion->save();
        
        // Get The Messages 
        $msgSuccess = "تمت اضافة ترويج الحلوى بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة ترويج الحلوى";
        Flashy::success($handel == true ? $msgSuccess : $msgFailure);
 
         if($handel == true ){
             foreach(\App\User::pluck('firebase_token')->toArray() as $record)
                     NotificationController::sendFCMEdit("تم اضافة عروض جديدة","New offers added","offer",$record);
             foreach(\App\Driver::pluck('firebase_token')->toArray() as $record)
                     NotificationController::sendFCMEdit("تم اضافة عروض جديدة","New offers added","offer",$record);
         }
        // Get Redirected
        return redirect('/desserts/dessertShow/'. $dessertId);   
    }
    
    public function dessertPromotionEdit ($dessertSizeId) {
        $Dessertpromotion = Dessertpromotion::where('dessertSizes_id', $dessertSizeId)->first();	
        return view('desserts.dessertPromotionEdit', compact('Dessertpromotion'));
    }
    
    public function dessertPromotionUpdate (Request $request, $dessertSizeId){
    
      	$rules = [
		    
     	   'discount_percentage'=>'required|numeric|min:1|max:99|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ];
        
        $messages = [
            'min'  =>' يجب ادخال الخصم بشكل صحيح '  ,
            'max'  =>' يجب ادخال الخصم بشكل صحيح '  ,
            'regex'  =>' يجب ادخال الخصم بشكل صحيح '  ,
            'numeric'  =>' يجب ادخال الخصم بشكل صحيح '  ,

        ];
       
        
        //Validate
      $errors = Validator::make($request->all(), $rules, $messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
        
        //Validate
        $errors = Validator::make($request->all(), $this->rules, $this->messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record

        $Dessertpromotion =  Dessertpromotion::where('dessertSizes_id', $dessertSizeId)->first();
        //$Dessertpromotion->dessertSizes_id = $request->dessertSizes_id;
        $Dessertpromotion->discount_percentage = $request->discount_percentage;
        $handel = $Dessertpromotion->save();
        
        // Get The Messages
        $msgSuccess = "تم تعديل الترويج بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل الترويج";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
       
      if($handel == true ){
             foreach(\App\User::where('is_active',1)->where('is_verified',1)->get() as $record)
                     NotificationController::sendFCMEdit("تم اضافة عروض جديدة ","New offers added","offer",$record->firebase_token);
             foreach(\App\Driver::where('is_active',1)->where('is_accepted',1)->get() as $record)
                     NotificationController::sendFCMEdit("","New offers added","offer",$record->firebase_token);
         }
        
        // Get Redirected
        return redirect('/desserts/dessertShow/'.$Dessertpromotion->Dessertsize->desserts_id);   
    } 

    
    public function dessertPromotionDestroy ($id){
    
      // Delete the Record
	  $handel = Dessertpromotion::destroy($id);
      
      // Get the Messages
      $msgSuccess = "تم حذف ترويج الحلوى بنجاح";
      $msgFailure = "عذرا! لم يتم حذف ترويج الحلوى";
      Flashy::success($handel == true ? $msgSuccess : $msgFailure);

      // Get Redirected
      return back();
    }

    
    public function desertPrmmtionUpdate($desertSizeID){
    		
              $this->validate($request, [
              		'per'=>'required|integer'
                ]);
                
			$DesertProtion=DesertProtion::find($desertSizeID);
           	if($DesertProtion == NULL){
            	$new=new DesertProtion();
                $new->dessertSizes_id=$desertSizeID;
             $new->discount_percentage=$request->per;
           } else {
            	$DesertProtion->discount_percentage=$request->per;
                
                 $DesertProtion->save();
        }    
                
        // Get the Messages    
        $msgSuccess = "تم تعيين النسبة المئوية بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة الحلوى";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);  
                    
	    return redirect('desserts/profile/'.$desertID);

    }
    

//{dessertId}/{dessertSizeId}
       
    public function desertsSizeedit($id){
    	$Dessertsize = Dessertsize::find($id);
    	return view('desserts.desertsSizeedit',['Dessertsize'=>$Dessertsize]);
    }
    
    //public function desertsSizeedit($dessertId, $dessertSizeId){
    	//$Dessertsize = Dessertsize::find($dessertId);
    	//return view('desserts.desertsSizeedit', compact('dessertId', 'dessertSizeId'));
    //}
    public function desertsSizeupdate(Request $request,$id,$desserts_id){

			$rules = [
		    
	        'description_ar'=>'required|min:3|max:40',
	        'description_en'=>'required|min:3|max:40',
			'price'=>'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
			'cock_time'=>'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/', 
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',	
            ];
        
      $messages = [
            'min'  =>' يجب ادخال ان لا يقل عن 3 حروف '  ,
            'max'  =>' يجب ان لا يزيد عن 40 حرف '  ,
            'regex'  =>' يجب ادخال الرقم بشكل صحيح '  ,
            'numeric'  =>' يجب ادخال الرقم بشكل صحيح '  ,
            'required'  =>' يجب ادخال قيمة '  ,
            'image'  =>' يجب ادخال صورة حقيقية '  ,
            'mimes'  =>' يجب ادخال صورة حقيقية '  ,
            'max'  =>'حجم الصورة اكبر من 1 ميجا  '  ,

        ];
        
          //Validate
      $errors = Validator::make($request->all(), $rules, $messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
        
        if($request->file('image') == true){
        $file=$request->image;
		$ad_photo = str_random(30) . '.' . $file->getClientOriginalExtension();
		$file->move( public_path('uploades/DessertSizes') , $ad_photo);
		$url=Request()->root().'/uploades/DessertSizes/'.$ad_photo;	    
        }
        $Dessertsize=Dessertsize::find($id);
        $Dessertsize->description_ar=$request->description_ar;
        $Dessertsize->description_en=$request->description_en;
        if($request->file('image') == true)
        $Dessertsize->image=$url;
        $Dessertsize->price=$request->price;
        $Dessertsize->cock_time=$request->cock_time;
     
		
        
        $handel=$Dessertsize->save();
        $msgSuccess = "تمت تعديل الحلوى بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة الحلوى";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);

        // Data to Return
        return redirect("/desserts/dessertShow/$desserts_id");
    }
    
    
    public function dessertSizeDelete ($id){
    
      // Delete the Record
	  $handel = Dessertsize::destroy($id);
      
      // Get the Messages
      $msgSuccess = "تم حذف حجم الحلوى بنجاح";
      $msgFailure = "عذرا! لم يتم حذف حجم الحلوى";
      Flashy::success($handel == true ? $msgSuccess : $msgFailure);

      // Get Redirected
      return back();
    }



}
