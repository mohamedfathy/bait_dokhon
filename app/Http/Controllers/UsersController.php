<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Flashy;
use Validator;


class UsersController extends Controller
{
  
    public function index () {
    	$User = User::orderBy('id', 'DESC')->get();
    	return view('users.index', compact('User'));
    }

    public function show ($id) {
    	$User = User::find($id);
    	return view('users.show', compact('User'));
    }

    public function search (Request $request) {
        if($request->has('q')){
        $User =  User::where('first_name', 'LIKE', '%'.$request->q.'%')
        ->orWhere('last_name', 'LIKE', '%'.$request->q.'%')
        ->orWhere('phone', 'LIKE', '%'.$request->q.'%')
        ->orWhere('first_name', 'LIKE', '%'.$request->q.'%')
        ->get();
        }
        return view('users.index', compact('User'));
    }
    

    public function activation ($id){
        $User = User::find($id);
        if($User->is_active == 1) {
            $User->is_active = 0;
        } else {
            $User->is_active = 1;
        }
        $User->save();
        $msgSuccess = " تم تفعيل المستخدم بنجاح ";
        $msgFailure = " تم تعطيل المستخدم بنجاح ";
        Flashy::success($User->is_active == 1 ? $msgSuccess : $msgFailure);
        return redirect('/users');
    }
}