<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cake;
use App\Sauce;
use App\Addon;

class Specialorder extends Model
{
    //
    protected $table = 'specialOrders';

    public function cake () {
    	return $this->belongsTo('App\Cake', 'cakes_id');
    }

    public function sauce () {
    	return $this->belongsTo('App\Sauce', 'sauces_id');
    }

    public function addon () {
    	return $this->belongsTo('App\Addon', 'addons_id');
    }
    public function Order () {
    	return $this->belongsTo('App\Order', 'orders_id');
    }

}
