<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Dessertsize;
use App\Addon;
class Normalorder extends Model
{
    //
    protected $table = 'normalOrders';

    public function dessertSize () {
    	return $this->belongsTo('App\Dessertsize', 'dessertSizes_id');
    }

    public function addon () {
    	return $this->belongsTo('App\Addon', 'addons_id');
    }
     public function Order () {
    	return $this->belongsTo('App\Order', 'orders_id');
    }
}
