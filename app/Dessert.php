<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Dessertsize;

class Dessert extends Model
{
    //
    public $timestamps = false;
    
    public function sizes()
    {
        return $this->hasMany('App\Dessertsize', 'id', 'dessertSizes_id');
    }
}
