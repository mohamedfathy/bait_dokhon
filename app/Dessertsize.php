<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Dessertpromotion;

class Dessertsize extends Model
{
    //
    public $table="dessertSizes";
    public $timestamps = false;
    
    public function promotion ()
    {
        return $this->belongsTo('App\Dessertpromotion', 'dessertSizes_id');
    }
      public function Dessert ()
    {
        return $this->belongsTo('App\Dessert', 'desserts_id');
    }
}
