<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class refusalReasons extends Model
{
  protected $table = 'refusalReasons';
  public $timestamps = false;
  
   public function order () {
    	return $this->belongsTo('App\Order', 'order_id');
    }
}
