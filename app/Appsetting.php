<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appsetting extends Model
{
    //
    protected $table = 'appSettings';
    public $timestamps = false;
}
