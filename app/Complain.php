<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Complain extends Model
{
    //
    protected $table = 'contact';

    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }

    public function driver()
    {
        return $this->belongsTo('App\Driver', 'drivers_id');
    }
}
