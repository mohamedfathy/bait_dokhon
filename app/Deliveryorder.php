<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Driver;
class Deliveryorder extends Model
{
    //
    protected $table = 'delivery_orders';

    public function driver () {
    	return $this->belongsTo('App\Driver', 'orders_id');
    }
}
