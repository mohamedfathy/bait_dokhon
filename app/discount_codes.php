<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class discount_codes extends Model
{
  protected $table = 'discount_codes';
  public $timestamps = false;
}
