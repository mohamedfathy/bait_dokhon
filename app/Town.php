<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
	public $table="towns";
    public $timestamps=false;
    public $primaryKey="townId";
    
   	public function city () {
    	return $this->belongsTo('App\City', 'cities_id');
    }
}
