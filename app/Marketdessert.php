<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marketdessert extends Model
{
    //
    public $timestamps=false;
    protected $table = 'markets_desserts';
}
