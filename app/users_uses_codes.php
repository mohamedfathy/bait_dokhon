<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users_uses_codes extends Model
{
  protected $table = 'users_uses_codes';
  public $timestamps = false;
}
