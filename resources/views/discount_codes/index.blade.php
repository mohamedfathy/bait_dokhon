@extends ('layouts.master')
@section('title', 'الخصومات')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
  <div class="page-header">
		
		<a href="/discount_codes/create" class="btn btn-primary btn-sm"> <i class="menu-icon fa fa-magic"></i> إضافة كود جديد  </a>

  </div><!--page-header-->
  <div class="row">
  <div class="col-xs-12">


	<div class="row">
	<div class="col-xs-12">
	<table id="users" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>    الكود</th>
            	<th> عدد مرات الاستخدام</th>
			<th class="center">الحالة</th>
			<th>تاريخ الاتنهاء</th>
			<th>تاريخ التسجيل</th>
			<th>  تعديل</th>
			<th>  مسح</th>
			</tr>
		</thead>
		
		<tbody>
			@if (count($discount_codes) == 0)
				<tr>
					<td colspan="8" style="text-align:center;" ><h3>لا توجد نتائج تطابق بحثك </h3></td>
				</tr>
			@endif
			@foreach ($discount_codes as $U)
			<tr> 
			<td>{{$U->code}} </td>
           		<td>{{$U->count_uses}}</td>
			<td class="center">
				@if ($U->is_active == 0)
				<a href="/discount_codes/activation/{{$U->id}}" class="btn btn-danger btn-xs">غير مفعل</a>
				@else
				<a href="/discount_codes/activation/{{$U->id}}" class="btn btn-success btn-xs">مفعل</a>
				@endif
			</td>
			<td>{{$U->end_date}}</td>
			<td>{{$U->created_at}}</td>
			<td><a href="/discount_codes/edit/{{$U->id}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
			<td><a href="/discount_codes/destroy/{{$U->id}}" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->


</div><!--col-xs-12-->
</div><!--row-->

</div><!-- /.page-content -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
<script type="text/javascript">
$(document).ready( function () {
    $('#users').DataTable();
} );
</script>
@endsection