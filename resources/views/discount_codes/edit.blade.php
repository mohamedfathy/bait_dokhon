@extends ('layouts.master')
@section('title', 'تعديل كود')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
<div class="row">
<div class="col-xs-12">

	<div class="row">
	<div class="col-xs-12">
	<div class="page-header">
      </div>
                  	{{ Form::open(['route' => 'discount_codes.update', 'class' => 'form']) }}
			@include('discount_codes.form2', ['btn' => 'حفظ', 'classes' => 'btn btn-primary'])
		{{ Form::close() }}
                   
                   
	</form>
	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.page-content-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection