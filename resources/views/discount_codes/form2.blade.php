<div class="col-xs-12"> 
<div class="form-group has-float-label col-sm-6">
	<label for="name">% نسبة الخصم </label>
    {{ Form::number('discount', $discount_code->discount, ['placeholder' => 'نسبة الخصم', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('discount') ? 'redborder' : '') ]) }}
    <small class="text-danger" style="font-weight:bold">{{ $errors->has('discount') ? $errors->first('discount') : '' }}</small>
</div>


<div class="form-group has-float-label col-sm-6">
	<label for="email"> تاريخ الانتهاء</label>
    {{ Form::date('end_date', Carbon\Carbon::parse($discount_code->end_date)->format("Y-m-d"), ['placeholder' => 'تاريخ الانتهاء  ', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('end_date') ? 'redborder' : '') ]) }}
    <small class="text-danger" style="font-weight:bold">{{ $errors->has('end_date') ? $errors->first('end_date') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
	<label for="name">   عدد مرات الاستخدام  </label>
    {{ Form::number('count_uses',$discount_code->count_uses, ['placeholder' => '  عدد مرات الاستخدام ', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('count_uses') ? 'redborder' : '') ]) }}
    <small class="text-danger" style="font-weight:bold">{{ $errors->has('count_uses') ? $errors->first('count_uses') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
    {{ Form::hidden('id',$discount_code->id, ['placeholder' => '  عدد مرات الاستخدام ', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('count_uses') ? 'redborder' : '') ]) }}
    <small class="text-danger" style="font-weight:bold">{{ $errors->has('count_uses') ? $errors->first('count_uses') : '' }}</small>
</div>

</div><!--col-xs-12-->

<div class="col-xs-12"> 
<div class="form-group col-sm-12 submit pull-left">
    <p>{{ Form::submit($btn , ['class' => 'btn btn-lg col-sm-3 pull-left btn-primary' . $classes ]) }}
</div>
</div><!--col-xs-12-->