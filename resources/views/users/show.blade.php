@extends ('layouts.master')
@section('title', 'المستخدم')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">

  <div class="page-header">
      <h1><i class="menu-icon fa fa-user"></i> المستخدم</h1>
  </div><!--page-header-->

<div class="row">
<div class="col-xs-12">

	<br>
	<div class="mydiv">
	<div id="user-profile-1" class="user-profile row">

		<div class="col-xs-12">
				<div class="space-12"></div><!-- /. space-12 -->
				<div class="profile-user-info profile-user-info-striped">

				<div class="profile-info-row">
					<div class="profile-info-name"> الاسم الاول</div><!--/.profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="username">{{$User->first_name}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> الاسم الاخير</div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="country">{{$User->last_name}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> الايميل </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="age">{{$User->email}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> رقم التليفون </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="signup">{{$User->phone}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> المدينة  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="login">{{$User->city->name_ar}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> القرية </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<i class="fa fa-map-marker light-orange bigger-110"></i>
					<span class="editable" id="about">{{$User->town}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> الحالة </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
						<span class="editable" id="about">
						@if ($User->is_active == 0)
						<span class="label label-danger">غير مفعل</span>
						@endif
						@if ($User->is_active == 1)
						<span class="label label-success">مفعل</span>@endif</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> تاريخ التسجيل </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$User->created_at}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				</div><!-- /. profile-user-info profile-user-info-striped -->
		</div><!-- /. col-xs-12 -->

	</div><!-- /# user-profile-1 -->
	</div><!-- /. mydiv -->

</div><!-- /.container -->
</div><!-- /.row -->
</div><!-- /.col-xs-12 -->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection