@extends ('layouts.master')
@section('title', 'المستخدمون')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<div class="row">
	<div class="col-xs-12">

	<form method="POST" action="/users/search" accept-charset="UTF-8" class="form">	

	<div class="col-lg-10 col-md-10 col-sm-10 col-sx-12">
	   {{ csrf_field() }}
       <input placeholder="اسم المستخدم / رقم التليفون" name="q" type="text" class="form-control search">
    </div><!-- col-xs-10 col-sm-10 col-md-10 col-lg-10 -->

    <div class="col-lg-2 col-md-2 col-sm-2 col-sx-12">
    	<input class="btn btn-success form-control" type="submit" value="بحث" >
    </div><!--col-xs-2-->
    </form>

	</div><!--col-xs-12-->
	</div><!--row-->
</div><!--breadcrumbs-->

<div class="page-content">
  <div class="page-header">
      <h1><i class="menu-icon fa fa-users"></i> المستخدمون</h1>
  </div><!--page-header-->
  <div class="row">
  <div class="col-xs-12">


	<div class="row">
	<div class="col-xs-12">
	<table id="users" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th> الاسم الاول</th>
            <th> الاسم الاخير</th>
			<th>الايميل</th>
			<th>التليفون</th>
			<th>المدينة</th>
			<th>القرية</th>
			<th class="center">الحالة</th>
			<th>تاريخ التسجيل</th>
			</tr>
		</thead>
		
		<tbody>
			@if (count($User) == 0)
				<tr>
					<td colspan="8" style="text-align:center;" ><h3>لا توجد نتائج تطابق بحثك </h3></td>
				</tr>
			@endif
			@foreach ($User as $U)
			<tr> 
			<td><a href="/users/{{$U->id}}">{{$U->first_name}} </a></td>
            <td>{{$U->last_name}}</td>
			<td>{{$U->email}}</td>
			<td>{{$U->phone}}</td>
			<td>{{$U->city->name_ar}}</td>
			<td>{{$U->town}}</td>
			@if ($U->is_active == 0)
			<td class="center">
			<a href="/users/activation/{{$U->id}}" class="btn btn-danger btn-xs">غير مفعل</a>
			</td>
			@endif
			@if ($U->is_active == 1)
			<td class="center">
			<a href="/users/activation/{{$U->id}}" class="btn btn-success btn-xs">مفعل</a>
			</td>
			@endif
			<td>{{$U->created_at}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->


</div><!--col-xs-12-->
</div><!--row-->

</div><!-- /.page-content -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
<script type="text/javascript">
$(document).ready( function () {
    $('#users').DataTable();
} );
</script>
@endsection