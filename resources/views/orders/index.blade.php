@extends ('layouts.master')
@section('title', 'الطلبات')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
<div class="row">
<div class="col-xs-12">

	<br>
	<div class="mydiv">
	<div id="user-profile-1" class="user-profile row">

		<!-- PAGE CONTENT BEGINS -->
		<div class="tabbable">

		<ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
			<li class="active">
				<a data-toggle="tab" href="#faq-tab-1" aria-expanded="true">
					<i class="blue ace-icon fa fa-pencil-square bigger-120"></i>
					الطلبات
				</a>
			</li>

			<li class="">
				<a data-toggle="tab" href="#faq-tab-2" aria-expanded="false">
					<i class="blue ace-icon fa fa-car bigger-120"></i>
					توصيل الطلبات
				</a>
			</li>

			<li class="">
				<a data-toggle="tab" href="#faq-tab-3" aria-expanded="false">
					<i class="blue ace-icon fa fa-times bigger-120"></i>
					الطلبات المرفوضة
				</a>
			</li>

			<li class="">
				<a data-toggle="tab" href="#faq-tab-4" aria-expanded="false">
					<i class="blue ace-icon fa fa-star bigger-120"></i>
					تقيمات الطلبات
				</a>
			</li>

			<li class="">
				<a data-toggle="tab" href="#faq-tab-5" aria-expanded="false">
					<i class="blue ace-icon fa fa-line-chart bigger-120"></i>
					العروض
				</a>
			</li>

			<li class="">
				<a data-toggle="tab" href="#faq-tab-6" aria-expanded="false">
					<i class="blue ace-icon fa fa-gift bigger-120"></i>
					الطلبات الخاصة
				</a>
			</li>
		</ul>

		<div class="tab-content no-border padding-24">
		<div id="faq-tab-1" class="tab-pane fade active in">
			<div class="space-8"></div>

				<div class="row">
				<div class="col-xs-12">
					<table id="orders" class="table table-bordered table-hover">
						<thead>
							<tr>
							<th>رقم الطلب</th>
							<th>اجمالي</th>
							<th>وقت الطهى</th>
							<th>الحالة</th>
                                          <th>نوع الطلب</th>
							<th>وقت الطلب</th>
							<th>الفرع</th>
							<th>المستخدم</th>
							<th>تليفون</th>
							<th>وقت التوصيل</th>
                        		      <th class="center">طباعة</th>
							<th class="center">حذف الطلب</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($Order as $O)
							<tr>
							<td><a href="/orders/{{$O->id}}">{{$O->code}}</a></td>
							<td>{{$O->price}}</td>
							<td>{{$O->cock_time}} دقيقة</td>
							<td class="center">
                            @if ($O->is_delivery == 1) 
                            
							@if ($O->status == 'waiting') 
							<a href="#" data-target="#deliveryModal" data-toggle="modal" class="deliveryClass label label-md label-warning " data-id="{{$O->id}}" ><i class="ace-icon fa fa-car "></i> انتظار </a>
							@elseif ($O->status == 'recieved')
							<a href="#" data-target="#deliveryModal" data-toggle="modal" class="deliveryClass label label-md label-success " data-id="{{$O->id}}" ><i class="ace-icon fa fa-car "></i> استقبال</a> 
							@elseif ($O->status == 'refused')
							<a href="#" data-target="#deliveryModal" data-toggle="modal" class="deliveryClass label label-md label-danger " data-id="{{$O->id}}" ><i class="ace-icon fa fa-car "></i> رفض</a>
							@else
							<a href="#" data-target="#deliveryModal" data-toggle="modal" class="deliveryClass label label-md label-primary " data-id="{{$O->id}}" ><i class="ace-icon fa fa-car "></i> انتهى</a>
							@endif
                            
                            @else
                            
                            @if ($O->status == 'waiting') 
							<a href="#" data-target="#my_modal" data-toggle="modal" class="identifyingClass label label-md label-warning" data-id="{{$O->id}}" ><i class="ace-icon fa fa-user "></i> انتظار</a>
							@elseif ($O->status == 'recieved')
							<a href="#" data-target="#my_modal" data-toggle="modal" class="identifyingClass label label-md label-success" data-id="{{$O->id}}" ><i class="ace-icon fa fa-user "></i> استقبال</a> 
							@elseif ($O->status == 'refused')
							<a href="#" data-target="#my_modal" data-toggle="modal" class="identifyingClass label label-md label-danger" data-id="{{$O->id}}" ><i class="ace-icon fa fa-user "></i> رفض</a>
							@else
							<a href="#" data-target="#my_modal" data-toggle="modal" class="identifyingClass label label-md label-primary" data-id="{{$O->id}}" ><i class="ace-icon fa fa-user "></i> انتهى</a>
							@endif
                            
                            @endif
							</td>
                            <td>
                            @if ($O->is_delivery == 1) 
                            توصيل للمنزل
                            @else
							استلام من الفرع
							@endif
                            </td>
							<td>{{$O->timestamp}}</td>
							<td>
							@if ($O->branches_id != null) 
							{{$O->branch->name_ar}}
							@else
							غير مذكور
							@endif
							</td>
                            
							<td>
							@if ($O->users_id != null) 
							<a href="/users/{{$O->users_id}}"> 
							{{$O->user->first_name}} {{$O->user->last_name}}
							</a></td>
							@else
							غير مذكور
							@endif
							</td>
								<td>
							@if ($O->users_id != null) 
						       {{$O->user->phone}}
						      </td>
							@else
							غير مذكور
							@endif
							</td>
							<td> 
							من :
					<br>
						{{Carbon\Carbon::parse(	$O->Deliveryorder->first()->delivery_from??"")->format('m-d H:i') }}	 
								<hr>
								الي :
						<br>
						{{Carbon\Carbon::parse(	$O->Deliveryorder->first()->delivery_to??"")->format('m-d H:i') }}	 
							</td>
                            <td class="center"><a class="label label-md label-primary" href="/orders/printView/{{$O->id}}" target="_blank"><i class="ace-icon fa fa-print align-top bigger-100 icon-on-right"></i> طباعة</a></td>
							<td class="center"><a href="/orders/orderDestroy/{{$O->id}}" class="label label-sm label-danger">حذف</a></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div><!-- /.col-xs-12 -->
				</div><!-- /.row -->

		</div><!--faq-tab-1-->

		<div id="faq-tab-2" class="tab-pane fade">
			<div class="space-8"></div>

				<div class="row">
				<div class="col-xs-12">
					<table id="simple-table" class="table table-bordered table-hover">
							<thead>
							<tr>
							<th>رقم الطلب</th>
							<th>السعر</th>
							<th>وقت الطهى</th>
							<th>الحالة</th>
							<th>وقت الطلب</th>
							<th>الفرع</th>
							<th>المستخدم</th>
							<th>تليفون</th>
							<th>وقت التوصيل</th>
                        		      <th class="center">طباعة</th>
							<th class="center">حذف الطلب</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($DeliveryOrder as $O)
							<tr>
							<td><a href="/orders/{{$O->id}}">{{$O->code}}</a></td>
							<td>{{$O->price}}</td>
							<td>{{$O->cock_time}} دقيقة</td>
                            <td>
                            @if ($O->is_delivery == 1) 
                            توصيل للمنزل
                            @else
							استلام من الفرع
							@endif
                            </td>
							<td>{{$O->timestamp}}</td>
							<td>
							@if ($O->branches_id != null) 
							{{$O->branch->name_ar}}
							@else
							غير مذكور
							@endif
							</td>
                            
							<td>
							@if ($O->users_id != null) 
							<a href="/users/{{$O->users_id}}"> 
							{{$O->user->first_name}} {{$O->user->last_name}}
							</a></td>
							@else
							غير مذكور
							@endif
							</td>
								<td>
							@if ($O->users_id != null) 
						       {{$O->user->phone}}
						      </td>
							@else
							غير مذكور
							@endif
							</td>
							<td> 
							من :
					<br>
						{{Carbon\Carbon::parse(	$O->Deliveryorder->first()->delivery_from??"")->format('m-d H:i') }}	 
								<hr>
								الي :
						<br>
						{{Carbon\Carbon::parse(	$O->Deliveryorder->first()->delivery_to??"")->format('m-d H:i') }}	 
							</td>
                            <td class="center"><a class="label label-md label-primary" href="/orders/printView/{{$O->id}}" target="_blank"><i class="ace-icon fa fa-print align-top bigger-100 icon-on-right"></i> طباعة</a></td>
							<td class="center"><a href="/orders/orderDestroy/{{$O->id}}" class="label label-sm label-danger">حذف</a></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div><!-- /.col-xs-12 -->
				</div><!-- /.row -->
        
		</div>

		<div id="faq-tab-3" class="tab-pane fade">
			<div class="space-8"></div>

				<div class="row">
				<div class="col-xs-12">
					<table id="simple-table" class="table table-bordered table-hover">
						<thead>
							<tr>
							<th>#</th>
							<th>الرسالة</th>
							<th>المستخدم</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($RefusedOrder as $RO)
							<tr>
							<td><a href="/orders/{{$RO->order_id}}">{{$RO->order->code}}</a></td>
							<td>{{$RO->content}}</td>
							<td>
								@if ($RO->order->users_id != null)
									<a href="/users/{{$RO->order->users_id}}"> 
										{{$RO->order->user->first_name}} {{$RO->order->user->last_name}}
									</a>
								@endif
							</td>		
							</tr>
							@endforeach
						</tbody>
					</table>
				</div><!-- /.col-xs-12 -->
				</div><!-- /.row -->
        
		</div>

		<div id="faq-tab-4" class="tab-pane fade">
			<div class="space-8"></div>

				<div class="row">
				<div class="col-xs-12">
					<table id="simple-table" class="table table-bordered table-hover">
						<thead>
							<tr>
							<th>#</th>
							<th>الرسالة</th>
							<th>التقيم</th>
							<th>المستخدم</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($OrderReview as $OR)
							<tr>
							<td>{{$OR->orders_id}}</td>
							<td>{{$OR->content}}</td>
							<td><i class="orange ace-icon fa fa-star"></i> {{$OR->rating}} </td>
							<td><a href="/users/{{$OR->userId}}" >{{$OR->userFirstName}} {{$OR->userLastName}}</a></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div><!-- /.col-xs-12 -->
				</div><!-- /.row -->
        
		</div>

		<div id="faq-tab-5" class="tab-pane fade">
			<div class="space-8"></div>

				<div class="row">
				<div class="col-xs-12">
					<table id="simple-table" class="table table-bordered table-hover">
						<thead>
							<tr>
							<th>#</th>
							<th>الحجم</th>
							<th>الاضافات</th>
							<th>الكمية</th>
							<th>رقم الطلب</th>
							<th>المستخدم</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($NormalOrder as $NO)
							<tr>
								<td>{{$NO->id}}</td>
								<td>{{$NO->dessertSize->description_ar??""}}</td>
								<td>{{$NO->addon->name_ar??""}}</td>
								<td>{{$NO->quantity}}</td>
								<td><a href="/orders/{{$NO->orders_id}}">{{$NO->Order->code??""}}</a></td>
								<td>
									@if($NO->Order != null)
										<a href="/users/{{$NO->Order->users_id}}" >
										{{$NO->Order->User->first_name}} {{$NO->Order->User->last_name}}</a>
									@endif	
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div><!-- /.col-xs-12 -->
				</div><!-- /.row -->
        
		</div>

		<div id="faq-tab-6" class="tab-pane fade">
			<div class="space-8"></div>

				<div class="row">
				<div class="col-xs-12">
					<table id="simple-table" class="table table-bordered table-hover">
						<thead>
							<tr>
							<th>#</th>
							<th>الكيكة</th>
							<th>الصوص</th>
							<th>الاضافات</th>
							<th>الطلب</th>
							<th>الكمية</th>
							<th>المستخدم</th>

							</tr>
						</thead>
						<tbody>
							@foreach ($SpecialOrder as $SO)
							<tr>
								<td>{{$SO->id}}</td>
								<td>{{$SO->cake->name_ar??""}}</td>
								<td>{{$SO->sauce->name_ar??""}}</td>
								<td>{{$SO->addon->name_ar??""}}</td>
								<td><a href="/orders/{{$SO->orders_id}}">{{$SO->Order->code??""}}</a></td>
								<td>{{$SO->quantity}}</td>
								<td>
										@if($SO->Order != null)
											<a href="/users/{{$SO->Order->users_id}}" >
											{{$SO->Order->User->first_name}} {{$SO->Order->User->last_name}}</a>
										@endif	
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div><!-- /.col-xs-12 -->
				</div><!-- /.row -->
        
		</div>

		</div>
		</div>
		<!-- PAGE CONTENT ENDS -->
		@include('orders.Modal')
	</div><!-- /# user-profile-1 -->
	</div><!-- /. mydiv -->

</div><!-- /.page-content -->
</div><!-- /.row -->
</div><!-- /.col-xs-12 -->
	
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
<script type="text/javascript">
$(document).ready( function () {

    $('#orders').DataTable( {	
        "order": [[ 0, "desc" ]]
    } );


} );
</script>
@endsection