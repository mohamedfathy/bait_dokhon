<div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-labelledby="my_modalLabel">
<div class="modal-dialog" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">تغيير الحالة</h4>
        </div>
        <div class="modal-body">
        	<div class="row">
	<div class="col-xs-6 col-sm-6">
		<a id="anchorWaiting" href="" class="btn btn-warning btn-block btn-xs">انتظار</a>
	</div>
	<div class="col-xs-6 col-sm-6">
		<a id="anchorRefused" href="" class="btn btn-danger  btn-block btn-xs Refused">رفض</a>
	</div>
	<hr>
	<div class="col-xs-6 col-sm-6">
		<a id="anchorRecieved" href="" class="btn btn-success  btn-block btn-xs">استقبال</a>
	</div>
	<div class="col-xs-6 col-sm-6">
		<a id="anchorFinished" href="" class="btn btn-primary  btn-block btn-xs">انتهى</a>
	</div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(function () {
        $(".identifyingClass").click(function () {
            var ID = $(this).data('id');
            $(".modal-body #hiddenValue").val(ID);
            var Mylink = "myLink" + ID;
    document.getElementById("anchorWaiting").setAttribute("href", "/orders/changeOrderStatus/waiting/"+ ID);
    document.getElementById("anchorRefused").setAttribute("href", "/orders/changeOrderStatus/refused/"+ ID);
    document.getElementById("anchorRecieved").setAttribute("href", "/orders/changeOrderStatus/recieved/"+ ID);
    document.getElementById("anchorFinished").setAttribute("href", "/orders/changeOrderStatus/finished/"+ ID);
        })
    });
</script>


<div class="modal fade" id="deliveryModal" tabindex="-1" role="dialog" aria-labelledby="my_modalLabel">
<div class="modal-dialog" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">سبب الرفض</h4>
        </div>
        <div class="modal-body">
        <div class="row">
	<div class="col-xs-6 col-sm-6">
		<a id="deliveryAnchorRefused" href="" class="btn btn-danger  btn-block btn-xs Refused">رفض</a>
	</div>

	<div class="col-xs-6 col-sm-6">
		<a id="deliveryAnchorFinished" href="" class="btn btn-primary  btn-block btn-xs">انتهى</a>
	</div>
        </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(function () {
        $(".deliveryClass").click(function () {
            var ID = $(this).data('id');
            $(".modal-body #hiddenValue").val(ID);
            var Mylink = "myLink" + ID;
    document.getElementById("deliveryAnchorRefused").setAttribute("href", "/orders/changeOrderStatus/refused/"+ ID);
    document.getElementById("deliveryAnchorFinished").setAttribute("href", "/orders/changeOrderStatus/finished/"+ ID);
        })
    });
</script>