@extends ('layouts.master')
@section('title', 'الطلب')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">

  <div class="page-header">
      <h1><i class="menu-icon fa fa-user"></i> الطلب</h1>
  </div><!--page-header-->

<div class="row">
<div class="col-xs-12">

	<br>
	<div class="mydiv">
	<div id="user-profile-1" class="user-profile row">

		<div class="col-xs-12">
				<div class="space-12"></div><!-- /. space-12 -->
				<div class="profile-user-info profile-user-info-striped">

				<div class="profile-info-row">
					<div class="profile-info-name"> اسم صاحب الطلب </div><!--/.profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="username">{{$ORDER->user->first_name}}  {{$ORDER->user->last_name}} </span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->


				<div class="profile-info-row">
					<div class="profile-info-name"> رقم  صاحب الطلب </div><!--/.profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="username">{{$ORDER->user->phone}}   </span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> وقت الطبخ  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
						<span class="label ">{{$ORDER->cock_time}} </span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->


				<div class="profile-info-row">
					<div class="profile-info-name" style="background:#ccc;color:blue;font-weight:bold"> خدمة التوصيل </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
						<span class="editable" id="about">
						@if ($ORDER->is_delivery == 0)
						<span class="label label-danger">لا </span>
						@endif
						@if ($ORDER->is_delivery == 1)
						<span class="label label-success">نعم</span>@endif</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->
				
			@if($ORDER->Deliveryorder->first() <> null)
				<div class="profile-info-row">
					<div class="profile-info-name" style="background:#ccc;color:blue;font-weight:bold">  العنوان </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->Deliveryorder->first()->address_ar??""}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name" style="background:#ccc;color:blue;font-weight:bold"> نوع مكان التوصيل </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">
			       	@if($ORDER->Deliveryorder->first() != null)
						@if($ORDER->Deliveryorder->first()->types=='home') منزل 
						@elseif($ORDER->Deliveryorder->first()->types=='flat') شقة 
					      @else مكتب	
					      @endif
				      @endif
				      </span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name" style="background:#ccc;color:blue;font-weight:bold"> رقم الطابق  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->Deliveryorder->first()->floor_number??""}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name" style="background:#ccc;color:blue;font-weight:bold"> رقم المكتب  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->Deliveryorder->first()->officeNumber??""}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name" style="background:#ccc;color:blue;font-weight:bold">  ملاحظات    </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->Deliveryorder->first()->notes??""}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name" > اسم  الفرع </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->Branch->name_ar??""}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> عنوان  الفرع </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->Branch->address_ar??""}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> سعر التوصيل </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->Deliveryorder->first()->price??""}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->
               
                  @endif

				<div class="profile-info-row">
					<div class="profile-info-name"> تاريخ الطلب </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->timestamp}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->


				<div class="profile-info-row">
					<div class="profile-info-name">  السعر </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->price/(\App\Appsetting::first()->fees /100 + 1) - ( count($ORDER->Deliveryorder) < 1 ? 0 :  $ORDER->Deliveryorder->first()->price ) }}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> ضريبة القيمة المضافة  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{\App\Appsetting::first()->fees}} %</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

			

				<div class="profile-info-row">
					<div class="profile-info-name">  الاجمالي </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->price}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->


				<div class="profile-info-row">
					<div class="profile-info-name"> رقم الطلب  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->code}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->


			
                 @if($ORDER->Normalorder->first() <> null)
				<div class="profile-info-row">
					<div class="profile-info-name">   المنتجات  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
                               @foreach($ORDER->Normalorder as $Normalorder)	
                               <span class="editable" id="about">
           			            <div class="clearfix"></div>

					       <span style="font-weight:bold">الاسم</span> :    {{$Normalorder->dessertSize->description_ar}}   &nbsp; &nbsp; &nbsp;
					        <span style="font-weight:bold">الكمية</span>  :    ( {{$Normalorder->quantity}} )  &nbsp; &nbsp; &nbsp;
					        <span style="font-weight:bold">الاضافة</span>  :     ( {{$Normalorder->addon->name_ar??" "}} ) 
     			            </span>
			            @endforeach
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->
                
				<div class="profile-info-row">
					<div class="profile-info-name"> اجمالي الكمية  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->Normalorder->sum('quantity')}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->
				
				
				<div class="profile-info-row">
					<div class="profile-info-name"> تابعة لعرض مروج  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
						<span class="editable" id="about">
					@if ($ORDER->Normalorder->first()->is_promotion == 0)
						<span class="label label-danger">لا </span>
					@else
						<span class="label label-success">نعم</span
					>@endif</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->
				
                  @endif


			<div class="profile-info-row">
				<div class="profile-info-name" style="background:#ccc;color:red;font-weight:bold">  طلب خاص </div><!-- /. profile-info-name -->
				<div class="profile-info-value">
					@if ($ORDER->specialOrders->first() == null)
					<span class="label label-danger">لا </span>
					@else 
					<span class="label label-success">نعم</span>@endif</span>
				</div><!-- /. profile-info-value -->
			</div><!-- /. profile-info-row -->
                     
                 @if($ORDER->specialOrders->first() <> null)
				<div class="profile-info-row">
					<div class="profile-info-name" style="background:#ccc;color:red;font-weight:bold">  الكيك </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->specialOrders->first()->cake->name_ar}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name" style="background:#ccc;color:red;font-weight:bold">   الصوص </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$ORDER->specialOrders->first()->sauce->name_ar}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

  
 				 @endif
				</div><!-- /. profile-user-info profile-user-info-striped -->
		</div><!-- /. col-xs-12 -->

	</div><!-- /# user-profile-1 -->
	</div><!-- /. mydiv -->

</div><!-- /.container -->
</div><!-- /.row -->
</div><!-- /.col-xs-12 -->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->

@endsection