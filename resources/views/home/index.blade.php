@extends ('layouts.master')
@section('title', 'الرئيسية')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">

  <div class="page-header">
      <h1><i class="menu-icon fa fa-tachometer"></i> الرئيسية</h1>
  </div><!--page-header-->

  <div class="row">
  <div class="col-xs-12">

    <div class="row">
    <div class="col-xs-12">
      <div class="statistics">
        <div class="statistics_box">
          <h1>{{$users}}  <i class="fa fa-user" aria-hidden="true"></i></h1>
          <h1>مستخدم</h1>
        </div><!--statistics_box-->
        <div class="statistics_box">
          <h1>{{$drivers}} <i class="fa fa-car" aria-hidden="true"></i></h1>
          <h1>سائق</h1>
        </div><!--statistics_box-->
          <div class="statistics_box">
          <h1>{{$orders}} <i class="fa fa-birthday-cake" aria-hidden="true"></i></h1>
          <h1>طلب</h1>
        </div><!--statistics_box-->
      </div><!--statistics-->
    </div><!--col-xs-12-->
    </div><!--row-->
        {!! Charts::styles() !!}

    <div class="row">
    <div class="col-xs-12">
        <div class="page-header">
        <h3 class="charts_header">اجمالى عدد المستخدمين : ({{$users}})</h3>
        </div>
        <div class="charts_border">
        {!! $chart_users->html() !!}
        </div><!--charts_border-->                 
    </div><!--col-xs-12-->
    </div><!--row-->

    <div class="row">
    <div class="col-xs-12">
      <div class="page-header">
         <h3 class="charts_header">اجمالى عدد السائقين : ({{$drivers}})</h3>
      </div>
      <div class="charts_border">
           {!! $chart_drivers->html() !!}
      </div><!--charts_border-->             
    </div><!--col-xs-12-->
    </div><!--row-->


    <div class="row">
    <div class="col-xs-12">
      <div class="page-header">
          <h3 class="charts_header">اجمالى عدد الطلبات : ({{$orders}})</h3>
      </div><!--page-header-->
      <div class="charts_border">
        {!! $chart_orders->html() !!}
      </div><!--charts_border-->             
    </div><!--col-xs-12-->
    </div><!--row-->


<br />

  <div class="row">
  <div class="col-xs-12 col-sm-6 widget-container-col" 
    id="widget-container-col-2">
    <div class="widget-box widget-color-blue" id="widget-box-2">

      <div class="widget-header">
        <h5 class="widget-title bigger lighter">
        <i class="fa fa-users" aria-hidden="true"></i>
        احدث المستخدمين
        </h5>
      </div><!--/.widget-header-->

      <div class="widget-body">
      <div class="widget-main no-padding">
        <table class="table table-striped table-bordered table-hover">
        <thead class="thin-border-bottom">
        <tr>
          <th><i class="ace-icon fa fa-user"></i> مستخدم </th>
          <th><i class="fa fa-calendar"></i> تاريخ التسجيل </th>
          <th class="hidden-480"><i class="fa fa-eye"></i> حالة المستخدم </th>
        </tr>
        </thead>

        <tbody>
        @foreach ($recent_users as $user)
        <tr>
          <td><a href="/users/{{$user->id}}">{{$user->first_name}} {{$user->last_name}}</a></td> 
          <td>{{$user->created_at}}</td>

          @if ($user->is_active == 1) 
            <td><span class="label label-success">مفعل</span></td>
          @endif
          @if ($user->is_active == 0) 
          <td><span class="label label-danger">غير مفعل</span></td>
          @endif
          </tr>
        @endforeach
        </tbody>
      </table><!--table table-striped-->

      </div><!-- /.widget-main -->
      </div><!-- /.widget-body -->
      
      </div><!-- /.widget-box -->
      </div><!-- /.col-xs-12 col-sm-6 widget-container-col-->


    <div class="col-xs-12 col-sm-6 widget-container-col" id="widget-container-col-2">
    <div class="widget-box widget-color-blue" id="widget-box-2">

      <div class="widget-header">
        <h5 class="widget-title bigger lighter">
        <i class="fa fa-car" aria-hidden="true"></i> احدث السائقين </h5>
      </div><!-- /.widget-header-->

      <div class="widget-body">
      <div class="widget-main no-padding">
      <table class="table table-striped table-bordered table-hover">

        <thead class="thin-border-bottom">
        <tr>
          <th><i class="ace-icon fa fa-user"></i> سائق </th>
          <th><i class="fa fa-calendar"></i> تاريخ التسجيل </th>
          <th class="hidden-480"><i class="fa fa-eye"></i> حالة السائق </th>
        </tr>
        </thead>
        <tbody>
        @foreach ($recent_drivers as $driver)
        <tr>
        <td><a href="/drivers/{{$driver->id}}">{{$driver->first_name}} {{$driver->last_name}}</a></td> 
        <td>{{$driver->created_at}}</td>

        @if ($driver->is_active == 1) 
        <td><span class="label label-success">مفعل</span></td>
        @endif
        @if ($driver->is_active == 0) 
        <td><span class="label label-danger">غير مفعل</span></td>
        @endif
        </tr>
        @endforeach
      </tbody>
  </table>

  </div><!-- /.widget-main -->
  </div><!-- /.widget-body -->

  </div><!-- /.widget-box -->
  </div><!-- /.col-xs-12 col-sm-6 widget-container-col -->


  </div><!-- /.row -->
  </div><!-- /col-md-11 -->
  </div><!-- /.container -->

    {!! Charts::scripts() !!}
    {!! $chart_users->script() !!}
    {!! $chart_orders->script() !!}
    {!! $chart_drivers->script() !!}

</div><!-- /.container -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection		