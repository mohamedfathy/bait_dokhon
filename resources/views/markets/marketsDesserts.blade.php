@extends ('layouts.master')
@section('title', 'الحلويات المتوفرة بمنفذ التوزيع')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
  <div class="page-header">
      <h1><i class="menu-icon fa fa-birthday-cake"></i> الحلويات المتوفرة بمنفذ التوزيع </h1>
  </div><!--page-header-->
  <div class="row">
  <div class="col-xs-12">
  
<div class="row">
<div class="col-xs-12">
	<div class="page-header">
<!--  -->
		<a href="/markets/addMarketDessert/{{$id}}" data-target="#my_modal" data-toggle="modal" class="identifyingClass btn btn-primary btn-sm" data-id="1" >اضافة حلوى بالمتجر</a>
	</div>
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>الاسم عربى</th>
			<th>الاسم انجليزى</th>
            <th>الحجم</th>
			</tr>
		</thead>
		
		<tbody>
        @foreach ($Marketdessert->desserts as $dessert)
        	<tr>

			<td>{{ $dessert->name_ar }}</td>
			<td>{{ $dessert->name_en }}</td>
            <td>{{ $dessert->sizes[0]->description_ar }}</td>
			</tr>
        @endforeach
		</tbody>
	</table>

	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->

	@include('markets.Modal')
</div><!--col-xs-12-->
</div><!--row-->

</div><!-- /.page-content -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection