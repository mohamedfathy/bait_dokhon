<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>

<div id="sidebar" class="sidebar responsive ace-save-state">
<script type="text/javascript">
try{ace.settings.loadState('sidebar')}catch(e){}
</script>

<ul class="nav nav-list">
	<li class="{{ Request::is(Route::current()->getName() == 'home') ? 'active' : '' }}">
	<a href="{{ route('home') }}">
	<i class="menu-icon fa fa-tachometer"></i>
	<span class="menu-text"> الرئيسية </span>
	</a>
	</li>

	<li class="{{ Request::is('users*') ? 'active' : '' }}">
	<a href="{{ route('users') }}">
	<i class="menu-icon fa fa-users"></i>
	<span class="menu-text"> المستخدمون </span>
	</a>
	</li>

	<li class="{{ Request::is('drivers*') ? 'active open' : '' }}">
	<a href="#" class="dropdown-toggle">
	<i class="menu-icon fa fa-car"></i>
	<span class="menu-text">
	 السائقون 
	</span>
	<b class="arrow fa fa-angle-down"></b>
	</a>

	<b class="arrow"></b>

	<ul class="submenu">
	<li class="{{ Request::is(Route::current()->getName() == 'drivers') ? 'active' : '' }}">
	<a href="{{ route('drivers') }}">
	<i class="menu-icon fa fa-caret-right"></i>
	<i class="fa fa-id-card-o"></i>
	 كل السائقين
	</a>
	<b class="arrow"></b>
	</li>

	<li class="{{ Request::is(Route::current()->getName() == 'indexApplicants') ? 'active' : '' }}">
	<a href="{{ route('indexApplicants') }}">
	<i class="menu-icon fa fa-caret-right"></i>
	<i class="fa fa-id-card-o"></i>
	طلبات الانضمام
	</a>
	<b class="arrow"></b>
	</li>

	<li class="{{ Request::is(Route::current()->getName() == 'editRequest') ? 'active' : '' }}">
	<a href="{{ route('editRequest') }}">
	<i class="menu-icon fa fa-caret-right"></i>
	<i class="fa fa-id-card-o"></i>
	طلبات التعديل
	</a>
	<b class="arrow"></b>
	</li>
	</ul>
	</li>



	<li class="{{ Request::is('orders*') ? 'active' : '' }}">
	<a href="{{ route('orders') }}">
	<i class="menu-icon fa fa-pencil-square"></i>
	<span class="menu-text"> الطلبات </span>
	</a>
	</li>


	<li class="{{ Request::is('desserts*') ? 'active open' : '' }}">
	<a href="#" class="dropdown-toggle">
	<i class="menu-icon fa fa-birthday-cake"></i>
	<span class="menu-text">
	 الأصناف  
	</span>
	<b class="arrow fa fa-angle-down"></b>
	</a>

	<b class="arrow"></b>

	<ul class="submenu">
	<li class="{{ Request::is(Route::current()->getName() == 'desserts') ? 'active' : '' }}">
	<a href="{{ route('desserts') }}">
	<i class="menu-icon fa fa-caret-right"></i>
	<i class="fa fa-id-card-o"></i>
	الأصناف 
	</a>
	<b class="arrow"></b>
	</li>

	<li class="{{ Request::is(Route::current()->getName() == 'dessertExtra') ? 'active' : '' }}">
	<a href="{{ route('dessertExtra') }}">
	<i class="menu-icon fa fa-caret-right"></i>
	<i class="fa fa-id-card-o"></i>
	الزيادات
	</a>
	<b class="arrow"></b>
	</li>


	</ul>
	</li>

	<li class="{{ Request::is('branches*') ? 'active open' : '' }}">
	<a href="#" class="dropdown-toggle">
	<i class="menu-icon fa fa-map-marker"></i>
	<span class="menu-text">
	 الفروع 
	</span>
	<b class="arrow fa fa-angle-down"></b>
	</a>

	<b class="arrow"></b>

	<ul class="submenu">
	<li class="{{ Request::is(Route::current()->getName() == 'branches') ? 'active' : '' }}">
	<a href="{{ route('branches') }}">
	<i class="menu-icon fa fa-caret-right"></i>
	<i class="fa fa-id-card-o"></i>
	 الفروع
	</a>
	<b class="arrow"></b>
	</li>

	<li class="{{ Request::is(Route::current()->getName() == 'cities') ? 'active' : '' }}">
	<a href="{{ route('cities') }}">
	<i class="menu-icon fa fa-caret-right"></i>
	<i class="fa fa-id-card-o"></i>
	 المدن
	</a>
	<b class="arrow"></b>
	</li>

	</ul>
	</li>


    
    <li class="{{ Request::is(Route::current()->getName() == 'markets') ? 'active' : '' }}">
		<a href="{{ route('markets') }}">
		<i class="menu-icon fa fa-shopping-cart"></i>
		<span class="menu-text"> المتاجر </span>
		</a>
	</li>

    <li class="{{ Request::is(Route::current()->getName() == 'notification') ? 'active' : '' }}">
		<a href="{{ route('notification') }}">
		<i class="menu-icon fa fa-bell"></i>
		<span class="menu-text"> الإشعارات المخصصة </span>
		</a>
	</li>
	
	<li class="{{ Request::is(Route::current()->getName() == 'complain') ? 'active' : '' }}">
		<a href="{{ route('complain') }}">
		<i class="menu-icon fa fa-commenting-o"></i>
		<span class="menu-text"> الشكاوى و الاقتراحات </span>
		</a>
	</li>


	<li class="{{ Request::is(Route::current()->getName() == 'contact') ? 'active' : '' }}">
		<a href="{{ route('contact') }}">
		<i class="menu-icon fa fa-phone"></i>
		<span class="menu-text"> اتصل بنا </span>
		</a>
	</li>


	<li class="{{ Request::is(Route::current()->getName() == 'about') ? 'active' : '' }}">
		<a href="{{ route('about') }}">
		<i class="menu-icon fa fa-exclamation-circle"></i>
		<span class="menu-text"> عن الموقع </span>
		</a>
	</li>
	
	
	<li class="{{ Request::is(Route::current()->getName() == 'fees') ? 'active' : '' }}">
		<a href="{{ route('fees') }}">
		<i class="menu-icon fa fa-money"></i>
		<span class="menu-text">ضريبة القيمة  المضافة</span>
		</a>
	</li>






</ul><!-- /.nav-list -->

<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
</div>
</div>