@extends ('layouts.master')
@section('title', 'تعديل منفذ توزيع')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
<div class="row">
<div class="col-xs-12">

	<div class="row">
	<div class="col-xs-12">
    
	<div class="page-header">
		<h1><i class="menu-icon fa fa-magic"></i> تعديل منفذ توزيع</h1>
    </div>
    
    {{ Form::model($Market, ['route' => ['marketUpdate' , $Market->id], 'class' => 'form', 'method' => 'PUT', 'files' => 'true']) }}
    @include('markets.editForm', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'Market' => $Market])
    {{ Form::close() }}
        
	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->
    
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!--/.page-content-->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection