@extends ('layouts.master')
@section('title', 'منافذ التوزيع')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
  <div class="page-header">
      <h1><i class="menu-icon fa fa-shopping-cart"></i> منافذ التوزيع</h1>
  </div><!--page-header-->
  <div class="row">
  <div class="col-xs-12">
  
<div class="row">
<div class="col-xs-12">
	<div class="page-header">
		<a href="/markets/marketAdd" class="btn btn-primary btn-sm">اضافة منفذ توزيع</a>
	</div>
		
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>الاسم عربى</th>
			<th>الاسم انجليزى</th>
			<th>صورة المتجر</th>
			<th>المدينة</th>
            <th class="center">قائمة الاصناف</th>
			<th class="center">تعديل</th>
			<th class="center">حذف</th>
			</tr>
		</thead>
		
		<tbody>
		@foreach ($Market as $M) 
			<tr>
			<td>{{$M->name_ar}}</td>
			<td>{{$M->name_en}}</td>
            <td><a href="/uploades/Markets/{{basename($M->image)}}" class="label label-primary" >صورة منفذ توزيع</a> </td>
			<td>{{$M->city->name_ar}}</td>
            <td class="center"><a href="/markets/listMarketDessert/{{$M->id}}" class="btn btn-warning btn-xs">عرض الحلويات</a></td>
			<td class="center"><a href="/markets/marketEdit/{{$M->id}}" class="btn btn-success btn-xs">تعديل</a></td>
			<td class="center"><a href="/markets/marketDestroy/{{$M->id}}" class="btn btn-danger btn-xs">حذف</a></td>
			</tr>
		@endforeach
		</tbody>
	</table>

	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->


</div><!--col-xs-12-->
</div><!--row-->

</div><!-- /.page-content -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection