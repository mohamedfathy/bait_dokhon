<div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-labelledby="my_modalLabel">
<div class="modal-dialog" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">اضافة حلوى بمنفذ التوزيع</h4>
        </div>
        <div class="modal-body">
        	<div class="row">
	<div class="col-xs-12 col-sm-12">
	<table id="simple-table" class="table table-bordered table-hover">

        
        <tbody>

        @foreach ($Dessert as $D)
        <tr>
            <td>{{ $D->name_ar }}</td>
            <td>{{ $D->name_en }}</td>
            <td class="center">
            @if($Marketdessert->desserts->contains('id', $D->id))
            <a href="/markets/removeMarketDessert/{{$id}}/{{$D->id}}"  class="identifyingClass btn btn-danger btn-xs" >حذف</a>
            @else
            <a href="/markets/addMarketDessert/{{$id}}/{{$D->id}}"  class="identifyingClass btn btn-success btn-xs" >اضافة</a>
            @endif
            </td>
            </tr>
        @endforeach
        </tbody>
    </table>
	</div>
            </div>
        </div>
    </div>
</div>
</div>