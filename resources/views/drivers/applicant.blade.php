@extends ('layouts.master')
@section('title', 'طلبات الانضمام')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
  <div class="page-header">
      <h1><i class="menu-icon fa fa-car"></i> طلبات الانضمام</h1>
  </div><!--page-header-->
  <div class="row">
  <div class="col-xs-12">
  
<div class="row">
<div class="col-xs-12">
<table id="simple-table" class="table table-bordered table-hover">
	<thead>
		<tr>
		<th>الاسم الاول</th>
        <th>الاسم الاخير</th>
		<th>الايميل</th>
		<th>التليفون</th>
		<th>المدينة</th>
		<th>تاريخ التسجيل</th>
		<th>رقم السيارة</th>
		<th> صورة الهواية</th>
		<th>  الصورة الشخصية</th>
		<th> صورة السيارة</th>
		<th class="center">قبول</th> 
        <th class="center">حذف</th>
		</tr>
	</thead>
	
	<tbody>
		@foreach ($Driver as $D)
		<tr> 
		<td class="center"><a href="/drivers/{{$D->id}}">{{$D->first_name}}</a></td>
            <td>{{$D->last_name}}</a></td>
		<td>{{$D->email}}</td>
		<td>{{$D->phone}}</td>
		<td>{{$D->city->name_ar}}</td> 
		<td>{{$D->created_at}}</td>
		<td>{{$D->car_number}}</td>
		<td><a href="{{$D->photo}}" target="_blank"><img src="{{$D->photo}}" style="height:50px;width:50px"></img></a>
		</td>
		<td><a href="{{$D->identityImage}}" target="_blank"><img src="{{$D->identityImage}}" style="height:50px;width:50px"></img></a>
		<td>
		    @foreach($D->Carphoto as $carPhoto)
             	    	  <a href="{{$carPhoto->url}}" target="_blank"><img src="{{$carPhoto->url}}" class="Thumbnail" style="height:50px;width:50px"></img></a>
		               &#160;
                @endforeach
            </td>
        <td class="center">
			<a href="/drivers/apply/{{$D->id}}" class="btn btn-success btn-xs">قبول</a>
        </td>
        <td class="center">
			<a href="#" data-target="#rejectApplicant" data-toggle="modal" class="btn btn-danger btn-xs rejectApplicant" data-id="{{$D->id}}">رفض</a>
        </td>
		</tr>
		@endforeach
	</tbody>
	</table>
    <div class="paginate">
        {{ $Driver->links() }}
    </div>
	</div><!-- /.col-xs-12 -->
    </div><!-- /.row -->

	@include('drivers.Modal')
</div><!--col-xs-12-->
</div><!--row-->

</div><!-- /.page-content -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection