@extends ('layouts.master')
@section('title', 'السائقون')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<div class="row">
	<div class="col-xs-12">

	<form method="POST" action="/drivers/search" accept-charset="UTF-8" class="form">	

	<div class="col-lg-10 col-md-10 col-sm-10 col-sx-12">
	   {{ csrf_field() }}
       <input placeholder="اسم السائق / رقم التليفون" name="q" type="text" class="form-control search">
    </div><!-- col-xs-10 col-sm-10 col-md-10 col-lg-10 -->

    <div class="col-lg-2 col-md-2 col-sm-2 col-sx-12">
    	<input class="btn btn-success form-control" type="submit" value="بحث" >
    </div><!--col-xs-2-->
    </form>

	</div><!--col-xs-12-->
	</div><!--row-->
</div><!--breadcrumbs-->


<div class="page-content">
  <div class="page-header">
      <h1><i class="menu-icon fa fa-car"></i> السائقون</h1>
  </div><!--page-header-->
  <div class="row">
  <div class="col-xs-12">
  
<div class="row">
<div class="col-xs-12">
<table id="drivers" class="table table-bordered table-hover">
	<thead>
		<tr>
		<th>الاسم الاول</th>
        <th>الاسم الاخير</th>
		<th>الايميل</th>
		<th>التليفون</th>
		<th>المدينة</th>
		<th>تاريخ التسجيل</th>
		<th>رقم السيارة</th>
		<th class="center">حالة السائق</th>
		<th class="center">الحالة</th>
		</tr>
	</thead>
	
	<tbody>
		@if (count($Driver) == 0)
			<tr>
				<td colspan="8" style="text-align:center;" ><h3>لا توجد نتائج تطابق بحثك </h3></td>
			</tr>
		@endif
		@foreach ($Driver as $D)
		<tr> 
		<td><a href="/drivers/{{$D->id}}">{{$D->first_name}}</a></td>
        <td>{{$D->last_name}}</a></td>
		<td>{{$D->email}}</td>
		<td>{{$D->phone}}</td>
		<td>{{$D->city->name_ar}}</td>
		<td>{{$D->created_at}}</td>
		<td>{{$D->car_number}}</td>
		<td class="center">		
		@if ($D->is_online == 0)
		<i class="ace-icon fa fa-circle light-red"></i> .
		@else
		<i class="ace-icon fa fa-circle light-green"></i>
		@endif
		</td>

		<td class="center">
		@if ($D->is_active == 0)
		<a href="/drivers/activation/{{$D->id}}" class="btn btn-danger btn-xs">غير مفعل</a>
		@else
		<a href="/drivers/activation/{{$D->id}}" class="btn btn-success btn-xs">مفعل</a>
		@endif
		</td>
		</tr>
		@endforeach
	</tbody>
	</table>
	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->


</div><!--col-xs-12-->
</div><!--row-->

</div><!-- /.page-content -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
<script type="text/javascript">
$(document).ready( function () {
    $('#drivers').DataTable();
} );
</script>
@endsection