@extends ('layouts.master')
@section('title', ' طلبات التعديل')
@section ('content')
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			<div class="page-header">
				<h1><i class="menu-icon fa fa-car"></i> طلبات التعديل</h1>
			</div><!--page-header-->
			<div class="row">
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-12">
							<table id="DriverEdit" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>الاسم الاول</th>
								            <th>الاسم الاخير</th>
										<th>البيان</th>
										<th>القيمة السابقة</th>
										<th>القيمة الجديدة</th>
									      <th class="center">قبول</th>
									      <th class="center">رفض</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($DriverEdit as $DE)
										<tr>
											@php
											$field = $DE->field;
											@endphp
											<td><a href="/drivers/{{$DE->drivers_id}}"> {{$DE->driver->first_name}}</a></td>
									            <td>{{$DE->driver->last_name}}</td>
											<td>{{$DE->field}}</td>
											<td>
												@if($DE->field == 'car_photo1' ||$DE->field == 'car_photo2' || $DE->field == 'car_photo3' || $DE->field == 'car_photo4' )
												       	@if($DE->field == 'car_photo1')
									 	                	    	  <a href="{{$DE->driver->Carphoto[0]->url}}" target="_blank"><img src="{{$DE->driver->Carphoto[0]->url}}" class="Thumbnail" style="height:50px;width:50px"></img></a>
											            	@elseif($DE->field == 'car_photo2')
									 	                	    	  <a href="{{$DE->driver->Carphoto[1]->url}}" target="_blank"><img src="{{$DE->driver->Carphoto[1]->url}}" class="Thumbnail" style="height:50px;width:50px"></img></a>
											            	@elseif($DE->field == 'car_photo3')
									 	                	    	  <a href="{{$DE->driver->Carphoto[2]->url}}" target="_blank"><img src="{{$DE->driver->Carphoto[2]->url}}" class="Thumbnail" style="height:50px;width:50px"></img></a>
											            	@elseif($DE->field == 'car_photo4')
									 	                	    	  <a href="{{$DE->driver->Carphoto[3]->url}}" target="_blank"><img src="{{$DE->driver->Carphoto[3]->url}}" class="Thumbnail" style="height:50px;width:50px"></img></a>
												            @endif
												@else
													{{$DE->driver->$field}}
												@endif
												</td>
												<td>
													@if($DE->field == 'car_photo1' ||$DE->field == 'car_photo2' || $DE->field == 'car_photo3' || $DE->field == 'car_photo4' )
										             	    	  <a href="{{$DE->content}}" target="_blank"><img src="{{$DE->content}}" class="Thumbnail" style="height:50px;width:50px"></img></a>
													@else
														{{$DE->content}}
													@endif
												</td>
											      <td class="center"><a href="/drivers/acceptEditRequest/{{$DE->drivers_id}}/{{$DE->field}}/" class="btn btn-success btn-xs">قبول</a></td>
											      <td class="center"><a href="/drivers/refuseEditRequest/{{$DE->drivers_id}}/{{$DE->field}}/{{$DE->content}}" class="btn btn-danger btn-xs">رفض</a></td>
										</tr>
									@endforeach
								</tbody>
				</table>
						</div><!-- /.col-xs-12 -->
					</div><!-- /.row -->
				</div><!--col-xs-12-->
</div><!--row-->
		
		</div><!-- /.page-content -->
	</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
<script type="text/javascript">
$(document).ready( function () {
    $('#DriverEdit').DataTable();
} );
</script>
@endsection