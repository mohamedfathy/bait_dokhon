<div class="modal fade" id="rejectApplicant" tabindex="-1" role="dialog" aria-labelledby="my_modalLabel">
<div class="modal-dialog" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">سبب الرفض : ارسال ايميل</h4>
        </div>
        <div class="modal-body">
        	<div class="row">
              <div class="col-xs-12 col-sm-12">
                 <textarea class="form-control" id="rejectReason" rows="3"></textarea>
              </div>
            </div>
        </div>
        <div class="modal-footer">
        <a href="" id="idURL" class="btn btn-danger btn-sm SendMail">حذف</a>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(function () {
        $(".rejectApplicant").click(function () {
            var ID = $(this).data('id');
    		document.getElementById("idURL").setAttribute("href", "/drivers/deleteApplicant/"+ ID);
        })
    });
    $(function () {
        $(".SendMail").click(function () {
        var idURLElement = document.getElementById("idURL");
		var idURL = idURLElement.getAttribute("href");
        var rejectReason = document.getElementById("rejectReason").value;
        var fullURL = idURL + "/" + rejectReason ; 
        idURLElement.setAttribute("href", fullURL);
        })
    });
</script>