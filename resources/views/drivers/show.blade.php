@extends ('layouts.master')
@section('title', 'السائقين')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">

  <div class="page-header">
      <h1><i class="menu-icon fa fa-car"></i> السائق</h1>
  </div><!--page-header-->

<div class="row">
<div class="col-xs-12">

	<br>
	<div class="mydiv">
	<div id="user-profile-1" class="user-profile row">

		<div class="col-xs-12 col-sm-3 center">
				<span class="profile-picture">
				<img id="avatar" width="190px" height="230px" class="editable img-responsive" alt="{{$Driver->first_name}} {{$Driver->last_name}}" src="{{ asset((($Driver->photo == NULL) ? 'profile_company/google.png' : 'profile_company/'.$Driver->photo) ) }}" />
				</span>
				<div class="space-4"></div><!-- /. space-4 -->

				<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
					<div class="inline position-relative">
					<span class="white">{{$Driver->first_name}} {{$Driver->last_name}}</span>
					</div><!-- /. inline position-relative -->
				</div><!-- /. width-80 -->
			<div class="space-6"></div><!-- /. space-6 -->
		</div><!-- /. col-xs-12 col-sm-3 center -->


		<div class="col-xs-12 col-sm-9">
				<div class="space-12"></div><!-- /. space-12 -->
				<div class="profile-user-info profile-user-info-striped">

				<div class="profile-info-row">
					<div class="profile-info-name"> الاسم الاول </div><!--/.profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="username">{{$Driver->first_name}} </span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> الاسم الاخير </div><!--/.profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="username">{{$Driver->last_name}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> الايميل </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="age">{{$Driver->email}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> رقم التليفون </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="signup">{{$Driver->phone}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> المدينة  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="login">{{$Driver->city->name_ar}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->


				<div class="profile-info-row">
					<div class="profile-info-name"> الحالة </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
						<span class="editable" id="about">
						@if ($Driver->is_active == 0)
						<span class="label label-danger">غير مفعل</span>
						@endif
						@if ($Driver->is_active == 1)
						<span class="label label-success">مفعل</span>@endif</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> تاريخ التسجيل </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$Driver->created_at}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> رقم السيارة </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$Driver->car_number}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->


				<div class="profile-info-row">
					<div class="profile-info-name"> صورة الهوبة </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
                              <a href="{{$Driver->photo}}" target="_blank"> <img src="{{$Driver->photo}}" style="height:50px;width:50px"></img></a>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->
				<div class="profile-info-row">
					<div class="profile-info-name">  الصورة الشخصية  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
                              <a href="{{$Driver->identityImage}}" target="_blank"><img src="{{$Driver->identityImage}}" style="height:50px;width:50px"></img></a>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name">   صور السيارة  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
						@foreach($Driver->Carphoto as $carPhoto)
                         	    	  <a href="{{$carPhoto->url}}" target="_blank"><img src="{{$carPhoto->url}}" style="height:50px;width:50px"></img></a>
				               &#160;
				            @endforeach
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				</div><!-- /. profile-user-info profile-user-info-striped -->
		</div><!-- /. col-xs-12 col-sm-9 -->

	</div><!-- /# user-profile-1 -->
	</div><!-- /. mydiv -->

</div><!-- /.container -->
</div><!-- /.row -->
</div><!-- /.col-xs-12 -->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection