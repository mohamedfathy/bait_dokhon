@extends ('layouts.master')
@section('title', 'الشكاوى و الاقتراحات')
@section ('content')
<div class="main-content">
<div class="main-content-inner">


<div class="page-content">
  <div class="page-header">
      <h1><i class="fa fa-commenting-o"></i>الشكاوى و الاقتراحات</h1>
  </div><!--page-header-->
  
<div class="row">
<div class="col-xs-12">
	<div class="row">
	<div class="col-xs-12">
    
	<table id="MYTABLE" class="table table-bordered table-hover display">
		<thead>
			<tr>
			<th>الاسم الاول</th>
            <th>الاسم الاخير</th>
			<th class="center">الرسالة</th>
			<th>تاريخ الارسال</th>
			<th>سائق / عميل</th>
            <th class="center">حذف</th>
			</tr>
		</thead>
		
		<tbody>
			@foreach ($Complain as $C) 
			<tr>

			@if ($C->users_id !== NULL)
			<td><a href="/users/{{$C->users_id}}">{{$C->user->first_name}}</a></td>
            <td>{{$C->user->last_name}}</td>
			@else
			<td><a href="/drivers/{{$C->drivers_id}}">{{$C->driver->first_name}}</a></td>
			<td>{{$C->driver->last_name}}</td>
			@endif

			<td>{{$C->message}}</td>
			<td>{{$C->timestamp}}</td>

            <td>
			@if ($C->users_id !== NULL)
			<span class="badge badge-success">عميل</span>
			@else
			<span class="badge badge-success">سائق</span>
			@endif
			</td>

			<td class="center">
			<a href="/complain/complainDestroy/{{$C->id}}" class="btn btn-danger btn-xs">حذف</a>
			</td>
			</tr>
			@endforeach
		</tbody>
	</table>

	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->
    
</div><!--col-xs-12-->
</div><!--row-->
</div><!-- /.page-content -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
<script type="text/javascript">
$(document).ready( function () {
    $('#MYTABLE').DataTable();
} );
</script>

@endsection