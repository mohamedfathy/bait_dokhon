<!-- Code For Google Maps -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1kz6gxC9-ZrMKUATK4C4YdtmqqxBEG4o&callback=initialize"></script>
<script type="text/javascript">
// Initialize the Class
var geocoder = new google.maps.Geocoder();

//Set the Default Latitute And Longtiude for the map
var Lat = 21.269853975203116; 
var lang = 39.237139648437505; 

// Get the long and latitude Position
function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress('Cannot determine address at this location.');
    }
  });
}

// Update the long and latitude Position
function updateMarkerPosition(latLng) {
    lat = latLng.lat();
    lang = latLng.lng();
    
    document.getElementById('latitude').value = lat;
    document.getElementById('logitude').value = lang; 
}

// Update the Marker Address
function updateMarkerAddress(str) {
  document.getElementById('address').innerHTML = str;
}

// Initializition function
function initialize() {
  var latLng = new google.maps.LatLng(21.269853975203116, 39.237139648437505);
  var map = new google.maps.Map(document.getElementById('mapCanvas'), {
    zoom: 8,
    center: latLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var marker = new google.maps.Marker({
    position: latLng,
    title: 'Point A',
    map: map,
    draggable: true
  });

  // Update current position info.
  updateMarkerPosition(latLng);
  geocodePosition(latLng);

  // Add dragging event listeners.
  google.maps.event.addListener(marker, 'dragstart', function() {
    updateMarkerAddress('Dragging...');
  });

  google.maps.event.addListener(marker, 'drag', function() {
    //updateMarkerStatus('Dragging...');
    updateMarkerPosition(marker.getPosition());
  });

  google.maps.event.addListener(marker, 'dragend', function() {
    //updateMarkerStatus('Drag ended');
    geocodePosition(marker.getPosition());
  });
}

// Onload handler to fire off the app.
google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div class="col-xs-12">

<div class="form-group has-float-label col-sm-6">
    <label for="name_ar">الاسم عربى </label>
    {{ Form::text('name_ar', old('name_ar'), ['placeholder' => 'الاسم عربى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('name_ar') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('name_ar') ? $errors->first('name_ar') : '' }}</small>
</div>


<div class="form-group has-float-label col-sm-6">
    <label for="name_en">الاسم انجليزى</label>
    {{ Form::text('name_en', old('name_en'), ['placeholder' => 'الاسم انجليزى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('name_en') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('name_en') ? $errors->first('name_en') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
    <label for="address_ar">العنوان عربى</label>
    {{ Form::text('address_ar', old('address_ar'), ['placeholder' => 'العنوان عربى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('address_ar') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('address_ar') ? $errors->first('address_ar') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
    <label for="address_en">العنوان انجليزى</label>
    {{ Form::text('address_en', old('address_en'), ['placeholder' => 'العنوان انجليزى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('address_en') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('address_en') ? $errors->first('address_en') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
    <label for="latitude">خط الطول</label>
    {{ Form::text('latitude', old('latitude'), ['placeholder' => 'خط الطول', 'required' => 'required', 'readonly' => 'true', 'id' => 'latitude', 'class' => 'form-control ' . ($errors->has('latitude') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('latitude') ? $errors->first('latitude') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
    <label for="logitude">خط العرض</label>
    {{ Form::text('logitude', old('logitude'), ['placeholder' => 'خط العرض', 'required' => 'required', 'readonly' => 'true', 'id' => 'logitude', 'class' => 'form-control ' . ($errors->has('logitude') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('logitude') ? $errors->first('logitude') : '' }}</small>
</div>


</div><!--col-xs-12-->

<div class="col-xs-12"> 
<div class="col-xs-12"> 
  <div class="page-header">
      <h5><i class="menu-icon fa fa-map-marker"></i> احداثيات خرائط جوجل</h5>
  </div><!--page-header-->

  <div id="mapCanvas"></div>
  <div id="infoPanel">
    <b>اقرب عنوان متطابق:</b>
    <div id="address"></div>
  </div>

</div><!--col-xs-12-->
</div><!--col-xs-12-->

<div class="col-xs-12"> 
<div class="form-group col-sm-12 submit pull-left">
    {{ Form::submit($btn , ['class' => 'btn btn-lg col-sm-3 pull-left btn-primary' . $classes ]) }}
</div>
</div><!--col-xs-12-->