@extends ('layouts.master')
@section('title', 'المدن')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">

<div class="row">
<div class="col-xs-12">
	<div class="page-header">
	<h1><i class="menu-icon fa fa-map-marker"></i> المدن </h1>
  	</div>

<div class="row">
<div class="col-xs-12">
	<div class="page-header">
		<a href="/discount_codes/create" class="btn btn-primary btn-sm">اضافة مدينة</a>
	</div>
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>الاسم عربى</th>
			<th>الاسم انجليزى</th>
            <th class="center"> الاحياء</th>
			<th class="center">تعديل</th>
			<th class="center">حذف</th>
			</tr>
		</thead>
		
		<tbody>
		@foreach ($City as $C) 
			<tr>
			<td>{{$C->name_ar}}</td>
			<td>{{$C->name_en}}</td>
            <td class="center"><a href="/branches/listCityTowns/{{$C->cityId}}" class="btn btn-warning btn-xs">عرض الاحياء</a></td>
			<td class="center"><a href="/branches/cityEdit/{{$C->cityId}}" class="btn btn-success btn-xs">تعديل</a></td>
			<td class="center"><a href="/branches/cityDestroy/{{$C->cityId}}" class="btn btn-danger btn-xs">حذف</a></td>
			</tr>
		@endforeach
		</tbody>
	</table>

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!--/.page-content-->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection