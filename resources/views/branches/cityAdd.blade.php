@extends ('layouts.master')
@section('title', 'إضافة مدينة')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
<div class="row">
<div class="col-xs-12">

	<div class="row">
	<div class="col-xs-12">
	<div class="page-header">
		<h1><i class="menu-icon fa fa-magic"></i> إضافة مدينة</h1>
    </div>
		{{ Form::open(['route' => 'cityCreate', 'class' => 'form']) }}
			@include('branches.cityForm', ['btn' => 'حفظ', 'classes' => 'btn btn-primary'])
		{{ Form::close() }}
	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.page-content-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection