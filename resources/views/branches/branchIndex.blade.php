@extends ('layouts.master')
@section('title', 'الفروع')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">

<div class="row">
<div class="col-xs-12">
	<div class="page-header">
	<h1><i class="menu-icon fa fa-map-marker"></i> الفروع </h1>
  	</div>

<div class="row">
<div class="col-xs-12">
	<div class="page-header">
		<a href="/branches/branchAdd" class="btn btn-primary btn-sm">اضافة فرع</a>
	</div>
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>الاسم عربى</th>
			<th>الاسم انجليزى</th>
			<th>العنوان عربى</th>
			<th>العنوان انجليزى</th>
			<th>تعديل</th>
			<th>حذف</th>
			</tr>
		</thead>
		
		<tbody>
		@foreach ($Branch as $B) 
			<tr>
			<td>{{$B->name_ar}}</td>
			<td>{{$B->name_en}}</td>
			<td>{{$B->address_ar}}</td>
			<td>{{$B->address_en}}</td>
			<td><a href="/branches/branchEdit/{{$B->id}}" class="btn btn-success btn-xs">تعديل</a></td>
			<td><a href="/branches/branchDestroy/{{$B->id}}" class="btn btn-danger btn-xs">حذف</a></td>
			</tr>
		@endforeach
		</tbody>
	</table>

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!--/.page-content-->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection