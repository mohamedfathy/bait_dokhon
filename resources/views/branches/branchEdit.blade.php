@extends ('layouts.master')
@section('title', 'تعديل فرع')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
<div class="row">
<div class="col-xs-12">

	<div class="row">
	<div class="col-xs-12">
	<div class="page-header">
		<h1><i class="menu-icon fa fa-magic"></i> تعديل فرع</h1>
    </div>
		{{ Form::model($Branch, ['route' => ['branchUpdate' , $Branch->id], 'class' => 'form', 'method' => 'PUT']) }}
		@include('branches.branchEditForm', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'Branch' => $Branch])
		{{ Form::close() }}
	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.page-content-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection