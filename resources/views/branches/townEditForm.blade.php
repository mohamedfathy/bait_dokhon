<div class="col-xs-12">

<div class="form-group has-float-label col-sm-6">
    <label for="name_ar">الاسم عربى </label>
    {{ Form::text('name_ar', old('Town->name_ar'), ['placeholder' => 'الاسم عربى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('name_ar') ? 'redborder' : '') ]) }}
    <small class="text-danger" style="font-weight:bold">{{ $errors->has('name_ar') ? $errors->first('name_ar') : '' }}</small>
</div>


<div class="form-group has-float-label col-sm-6">
    <label for="name_en">الاسم انجليزى</label>
    {{ Form::text('name_en', old('Town->name_en'), ['placeholder' => 'الاسم انجليزى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('name_en') ? 'redborder' : '') ]) }}
    <small class="text-danger" style="font-weight:bold">{{ $errors->has('name_en') ? $errors->first('name_en') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
    <label for="delivery_price">سعر التوصيل</label>
    {{ Form::number('delivery_price', old('Town->delivery_price'), ['placeholder' => 'سعر التوصيل', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('delivery_price') ? 'redborder' : '') ]) }}
    <small class="text-danger" style="font-weight:bold">{{ $errors->has('delivery_price') ? $errors->first('delivery_price') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
    <label for="cities_id">المدينة</label>
    {{ Form::select('cities_id', $City, old('cities_id'), ['placeholder' => 'المدينة', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('cities_id') ? 'redborder' : '') ]) }}
    <small class="text-danger" style="font-weight:bold">{{ $errors->has('cities_id') ? $errors->first('cities_id') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
    <label for="latitude">خط الطول</label>
    {{ Form::text('latitude', old('latitude'), ['placeholder' => 'خط الطول', 'required' => 'required', 'readonly' => 'true', 'id' => 'latitude', 'class' => 'form-control ' . ($errors->has('latitude') ? 'redborder' : '') ]) }}
    <small class="text-danger" style="font-weight:bold">{{ $errors->has('latitude') ? $errors->first('latitude') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
    <label for="logitude">خط العرض</label>
    {{ Form::text('logitude', old('logitude'), ['placeholder' => 'خط العرض', 'required' => 'required', 'readonly' => 'true', 'id' => 'logitude', 'class' => 'form-control ' . ($errors->has('logitude') ? 'redborder' : '') ]) }}
    <small class="text-danger" style="font-weight:bold">{{ $errors->has('logitude') ? $errors->first('logitude') : '' }}</small>
</div>


</div><!--col-xs-12-->

<div class="col-xs-12"> 
<div class="col-xs-12"> 
  <div class="page-header">
      <h5><i class="menu-icon fa fa-car"></i> احداثيات خرائط جوجل</h5>
  </div><!--page-header-->

  <div id="mapCanvas"></div>
  <div id="infoPanel">
<!--     <b>Marker status:</b>
    <div id="markerStatus"><i>Click and drag the marker.</i></div> -->
<!--     <b>Current position:</b>
    <div id="info"></div> -->
    <b>اقرب عنوان متطابق:</b>
    <div id="address"></div>
  </div>

</div><!--col-xs-12-->
</div><!--col-xs-12-->

<div class="col-xs-12"> 
<div class="form-group col-sm-12 submit pull-left">
    {{ Form::submit($btn , ['class' => 'btn btn-lg col-sm-3 pull-left btn-primary' . $classes ]) }}
</div>
</div><!--col-xs-12-->

<!-- Code For Google Maps -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1kz6gxC9-ZrMKUATK4C4YdtmqqxBEG4o&callback=initialize"></script>
<script type="text/javascript">

// Initialize the Class
var geocoder = new google.maps.Geocoder();

// Get the value of location on maps
var Lat = document.getElementById('latitude').value; 
var lang = document.getElementById('logitude').value; 

// Get the long and latitude Position
function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress('Cannot determine address at this location.');
    }
  });
}

// Update the long and latitude Position
function updateMarkerPosition(latLng) {
    lat = latLng.lat();
    lang = latLng.lng();
    
    document.getElementById('latitude').value = lat;
    document.getElementById('logitude').value = lang; 
}

// Update the Marker Address
function updateMarkerAddress(str) {
  document.getElementById('address').innerHTML = str;
}

function initialize() {
  var latLng = new google.maps.LatLng(Lat, lang);
  var map = new google.maps.Map(document.getElementById('mapCanvas'), {
    zoom: 8,
    center: latLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var marker = new google.maps.Marker({
    position: latLng,
    title: 'Point A',
    map: map,
    draggable: true
  });

  // Update current position info.
  updateMarkerPosition(latLng);
  geocodePosition(latLng);

  // Add dragging event listeners.
  google.maps.event.addListener(marker, 'dragstart', function() {
    updateMarkerAddress('Dragging...');
  });

  google.maps.event.addListener(marker, 'drag', function() {
    // updateMarkerStatus('Dragging...');
    updateMarkerPosition(marker.getPosition());
  });

  google.maps.event.addListener(marker, 'dragend', function() {
    // updateMarkerStatus('Drag ended');
    geocodePosition(marker.getPosition());
  });
}

// Onload handler to fire off the app.
google.maps.event.addDomListener(window, 'load', initialize);
</script>