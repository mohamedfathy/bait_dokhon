@extends ('layouts.master')
@section('title', 'احياء المدينة')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">

<div class="row">
<div class="col-xs-12">
	<div class="page-header">
	<h1><i class="menu-icon fa fa-map-marker"></i> احياء المدينة </h1>
  	</div>

<div class="row">
<div class="col-xs-12">
	<div class="page-header">
		<a href="/branches/townAdd" class="btn btn-primary btn-sm">اضافة حى</a>
	</div>
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>الاسم عربى</th>
			<th>الاسم انجليزى</th>
			<th>سعر التوصيل</th>
			<th>المدينة</th>
			<th>تعديل</th>
			<th>حذف</th>
			</tr>
		</thead>
		
		<tbody>
		@foreach ($Town as $T) 
			<tr>
			<td>{{$T->name_ar}}</td>
			<td>{{$T->name_en}}</td>
			<td>{{$T->delivery_price}}</td>
			<td>{{$T->city->name_ar}}</td>
			<td><a href="/branches/townEdit/{{$T->townId}}" class="btn btn-success btn-xs">تعديل</a></td>
			<td><a href="/branches/townDestroy/{{$T->townId}}" class="btn btn-danger btn-xs">حذف</a></td>
			</tr>
		@endforeach
		</tbody>
	</table>

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!--/.page-content-->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection