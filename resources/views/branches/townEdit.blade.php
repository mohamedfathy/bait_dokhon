@extends ('layouts.master')
@section('title', 'تعديل حى')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
<div class="row">
<div class="col-xs-12">

	<div class="row">
	<div class="col-xs-12">
	<div class="page-header">
		<h1><i class="menu-icon fa fa-magic"></i>تعديل حى</h1>
    </div>
    	{{ Form::model($Town, ['route' => ['townUpdate', $Town->townId], 'class' => 'form', 'method' => 'PUT']) }}
			@include('branches.townEditForm', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'Town' => $Town])
		{{ Form::close() }}
	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.page-content-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection