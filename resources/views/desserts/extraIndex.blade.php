@extends ('layouts.master')
@section('title', 'الحلويات')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">

<div class="row">
<div class="col-xs-12">

<div class="row">
<div class="col-xs-12">

	<div id="user-profile-1" class="user-profile">

		<!-- PAGE CONTENT BEGINS -->
		<div class="tabbable">

		<ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
			<li class="active">
				<a data-toggle="tab" href="#faq-tab-1" aria-expanded="true">
					<i class="blue ace-icon fa fa fa-birthday-cake bigger-120"></i>
					الاضافات
				</a>
			</li>

			<li class="">
				<a data-toggle="tab" href="#faq-tab-2" aria-expanded="false">
					<i class="orange ace-icon fa fa-birthday-cake bigger-120"></i>
					الكيك
				</a>
			</li>

			<li class="">
				<a data-toggle="tab" href="#faq-tab-3" aria-expanded="false">
					<i class="red ace-icon fa fa-birthday-cake bigger-120"></i>
					الصوص
				</a>
			</li>
		</ul>

		<div class="tab-content no-border padding-24">
		<div id="faq-tab-1" class="tab-pane fade active in">
				<div class="page-header">
				<a href="/desserts/addonAdd" class="btn btn-primary btn-xs">اضافة اضافة</a>
				</div>
			<table id="simple-table" class="table table-bordered table-hover">
			<thead>
				<tr>
				<th>#</th>
				<th>الاسم عربى</th>
				<th>الاسم انجليزى</th>
				<th>السعر</th>
				<th>زمن الطهى</th>
				<th>تعديل</th>
				<th>حذف</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($Addon as $A)
				<tr>
				<td>{{$A->id}}</td>
				<td>{{$A->name_ar}}</td>
				<td>{{$A->name_en}}</td>
				<td>{{$A->price}}</td>
				<td>{{$A->cock_time}}</td>
				<td><a href="/desserts/addonEdit/{{$A->id}}" class="btn btn-success btn-xs">تعديل</a></td>
				<td><a href="/desserts/addonDestroy/{{$A->id}}" class="btn btn-danger btn-xs">حذف</a></td>
				</tr>
				@endforeach
			</tbody>
			</table>
		</div><!--faq-tab-1-->

		<div id="faq-tab-2" class="tab-pane fade">
			<div class="page-header">
				<a href="/desserts/cakeAdd" class="btn btn-primary btn-xs">اضافة كيكة</a>
			</div>
			<table id="simple-table" class="table table-bordered table-hover">
				<thead>
					<tr>
					<th>#</th>
					<th>الاسم عربى</th>
					<th>الاسم انجليزى</th>
					<th>السعر</th>
					<th>زمن الطهى</th>
					<th>تعديل</th>
					<th>حذف</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($Cake as $C)
					<tr>
					<td>{{$C->id}}</td>
					<td>{{$C->name_ar}}</td>
					<td>{{$C->name_en}}</td>
					<td>{{$C->price}}</td>
					<td>{{$C->cock_time}}</td>
					<td><a href="/desserts/cakeEdit/{{$C->id}}" class="btn btn-success btn-xs">تعديل</a></td>
					<td><a href="/desserts/cakeDestroy/{{$C->id}}" class="btn btn-danger btn-xs">حذف</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div><!--faq-tab-2-->

		<div id="faq-tab-3" class="tab-pane fade">
		<div class="page-header">
		<a href="/desserts/sauceAdd" class="btn btn-primary btn-xs">اضافة صوص</a>
		</div>
			<table id="simple-table" class="table table-bordered table-hover">
				<thead>
					<tr>
					<th>#</th>
					<th>الاسم عربى</th>
					<th>الاسم انجليزى</th>
					<th>السعر</th>
					<th>زمن الطهى</th>
					<th>تعديل</th>
					<th>حذف</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($Sauce as $S)
					<tr>
					<td>{{$S->id}}</td>
					<td>{{$S->name_ar}}</td>
					<td>{{$S->name_en}}</td>
					<td>{{$S->price}}</td>
					<td>{{$S->cock_time}}</td>
					<td><a href="/desserts/sauceEdit/{{$S->id}}" class="btn btn-success btn-xs">تعديل</a></td>
					<td><a href="/desserts/sauceDestroy/{{$S->id}}" class="btn btn-danger btn-xs">حذف</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>     
		</div><!--faq-tab-3-->
		
	</div><!-- /tab-content-->
	</div><!-- /tabbable-->
	<!-- PAGE CONTENT ENDS -->
	</div><!-- /# user-profile-1 -->

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!-- /.page-content -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection