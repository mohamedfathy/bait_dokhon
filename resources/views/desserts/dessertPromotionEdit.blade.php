@extends ('layouts.master')
@section('title', 'تعديل الترويج')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
<div class="row">
<div class="col-xs-12">

	<div class="row">
	<div class="col-xs-12">
	<div class="page-header">
		<h1><i class="menu-icon fa fa-magic"></i> تعديل الترويج</h1>
    </div>
		{{Form::model($Dessertpromotion ,['route' => ['dessertPromotionUpdate', $Dessertpromotion->dessertSizes_id],'class' => 'form']) }}
			@include('desserts.dessertPromotionForm', ['btn' => 'حفظ', 'classes' => 'btn btn-primary'])
		{{ Form::close() }}
	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.page-content-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection