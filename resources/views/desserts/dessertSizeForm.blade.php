<div class="col-xs-12">

<div class="form-group has-float-label col-sm-6">
    <label for="description_ar">الوصف عربى </label>
    {{ Form::text('description_ar', old('description_ar'), ['placeholder' => 'الوصف عربى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('description_ar') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('description_ar') ? $errors->first('description_ar') : '' }}</small>
</div>


<div class="form-group has-float-label col-sm-6">
    <label for="description_en">الوصف انجليزى</label>
    {{ Form::text('description_en', old('description_en'), ['placeholder' => 'الوصف انجليزى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('description_en') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('description_en') ? $errors->first('description_en') : '' }}</small>
</div>


<div class="form-group has-float-label col-sm-6">
    <label for="price">السعر</label>
    {{ Form::number('price', old(' price'), ['placeholder' => 'السعر', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('price') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('price') ? $errors->first('price') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
    <label for="cock_time">وقت الاعداد</label>
    {{ Form::number('cock_time', old('cock_time'), ['placeholder' => 'وقت الاعداد', 'required' => 'required', 'id' => 'cock_time', 'class' => 'form-control ' . ($errors->has('cock_time') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('cock_time') ? $errors->first('cock_time') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
    <label for="address_ar">صورة الصنف</label>
    <input type="file" name="image">
        <small class="text-danger">{{ $errors->has('image') ? $errors->first('image') : '' }}</small>

</div>

</div><!--col-xs-12-->

<div class="col-xs-12"> 
<div class="form-group col-sm-12 submit pull-left">
    {{ Form::submit($btn , ['class' => 'btn btn-lg col-sm-3 pull-left btn-primary' . $classes ]) }}
</div>
</div><!--col-xs-12-->