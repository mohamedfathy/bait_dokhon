@extends ('layouts.master')
@section('title', 'عرض التفاصيل')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">

<div class="row">
<div class="col-xs-12">
	<div class="page-header">
	<h1><i class="menu-icon fa fa-birthday-cake"></i> عرض التفاصيل </h1>
  	</div>
    
   	<div class="page-header">
    <h1><span class="badge badge-primary">الاسم عربى : {{$desert->name_ar}}</span> &nbsp;
    <span class="badge badge-primary">الاسم انجليزى : {{$desert->name_en}}</span></h1>
  	</div>

<div class="row">
<div class="col-xs-12">
	<div class="page-header">
		<a href="/desserts/dessertAddSize/{{$desert->id}}" class="btn btn-primary btn-sm">اضافة حجم جديد</a>
	</div>
    @if($desertSizes == NULL)
    <div class="alert alert-info">لا يوجد مقاسات لهذه الحلوى برجاء الاضافة</div>
    @else
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
            <th>الوصف عربى</th>
            <th>الوصف انجليزى</th>
            <th>صورة الحلوي</th>
            <th>السعر</th>
            <th>وقت اﻻعداد</th>
            <th>اﻻعدادات</th>
            <th>الترويج</th>
			</tr>
		</thead>
		
		<tbody>
        @foreach($desertSizes as $desertSize)
        @if($desert->dessertSizes_id != $desertSize->id)
        @endif
        <tr>
        <td>{{$desertSize->description_ar}}</td>
        <td>{{$desertSize->description_en}}</td>
        <td><a href="/uploades/DessertSizes/{{basename($desertSize->image)}}" class="label label-primary" >صورة الحلوى</a> </td>
        <td>{{$desertSize->price}} </td>
        <td>{{$desertSize->cock_time}}</td>
        <td>
        <!--<a href="/desert/edit/{{$desert->id}}/{{$desertSize->id}}" class="label label-success">تعديل</a>-->
        <a href="/desert/edit/{{$desertSize->id}}" class="label label-success">تعديل</a>
        @if($desert->dessertSizes_id != $desertSize->id)
        <a href="/desserts/dessertSizeDelete/{{$desertSize->id}}" class="label label-danger">مسح</a>
        <a href="/desserts/dessertSizeDefault/{{$desert->id}}/{{$desertSize->id}}" class="label label-default">حجم افتراضي</a>
        @endif
        </td>
		@if (in_array($desertSize->id, $DessertpromotionIds) == true ) 
   		<td><a href="/desserts/dessertPromotionEdit/{{$desertSize->id}}" class="label label-success"> تعديل </a>&nbsp;
        <a href="/desserts/dessertPromotionDestroy/{{$desertSize->id}}" class="label label-danger"> حذف  </a></td>
		@else 
        <td><a href="/desserts/dessertPromotionAdd/{{$desert->id}}/{{$desertSize->id}}" class="label label-primary"> اضافة  </a></td>
		@endif
        </tr>
        @endforeach
		</tbody>
	</table>
	@endif
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!--/.page-content-->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection