<div class="col-xs-12">

<div class="form-group has-float-label col-sm-6">
    <label for="discount_percentager"> الخصم </label>
    {{ Form::number('discount_percentage', old('Dessertpromotion->discount_percentage'), ['placeholder' => 'الخصم', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('discount_percentage') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('discount_percentage') ? $errors->first('discount_percentage') : '' }}</small>
</div>

</div><!--col-xs-12-->

<div class="col-xs-12"> 
<div class="form-group col-sm-12 submit pull-left">
    {{ Form::submit($btn , ['class' => 'btn btn-lg col-sm-3 pull-left btn-primary' . $classes ]) }}
</div>
</div><!--col-xs-12-->