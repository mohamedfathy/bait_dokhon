@extends ('layouts.master')
@section('title', 'اضافة حجم للصنف')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
<div class="row">
<div class="col-xs-12">

	<div class="row">
	<div class="col-xs-12">
	<div class="page-header">
		<h1><i class="menu-icon fa fa-birthday-cake"></i> اضافة حجم للصنف</h1>
    </div> 
		{{ Form::open(['route' => ['dessertCreateSize', $Dessert->id], 'class' => 'form', 'files' => 'true']) }}
			@include('desserts.dessertSizeForm', ['btn' => 'حفظ', 'classes' => 'btn btn-primary'])
		{{ Form::close() }}
	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.page-content-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection