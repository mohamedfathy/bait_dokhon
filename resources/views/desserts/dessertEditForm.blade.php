<div class="col-xs-12"> 
<div class="form-group has-float-label col-sm-6">
	<label for="name">الاسم عربى </label>
    {{ Form::text('name_ar', old('Dessert->name_ar'), ['placeholder' => 'الاسم عربى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('name_ar') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('name_ar') ? $errors->first('name_ar') : '' }}</small>
</div>


<div class="form-group has-float-label col-sm-6">
	<label for="email">الاسم انجليزى</label>
    {{ Form::text('name_en', old('Dessert->name_en'), ['placeholder' => 'الاسم انجليزى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('name_en') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('name_en') ? $errors->first('name_en') : '' }}</small>
</div>
</div><!--col-xs-12-->
<div class="col-xs-12"> 
<div class="form-group col-sm-12 submit pull-left">
    <p>{{ Form::submit($btn , ['class' => 'btn btn-lg col-sm-3 pull-left btn-primary' . $classes ]) }}
</div>
</div><!--col-xs-12-->