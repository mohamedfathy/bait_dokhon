<div class="col-xs-12"> 
<div class="form-group has-float-label col-sm-6">
	<label for="name">الاسم عربى </label>
    {{ Form::text('name_ar', old('dessert->name_ar'), ['placeholder' => 'الاسم عربى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('name_ar') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('name_ar') ? $errors->first('name_ar') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
	<label for="email">الاسم انجليزى</label>
    {{ Form::text('name_en', old('dessert->name_en'), ['placeholder' => 'الاسم انجليزى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('name_en') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('name_en') ? $errors->first('name_en') : '' }}</small>
</div>
</div><!--col-xs-12-->

<div class="col-xs-12"> 
    <div class="col-xs-12"> 
  <div class="page-header">
      <h5><i class="orange menu-icon fa fa-sliders"></i> الوصف</h5>
  </div><!--page-header-->
  </div>
  
<div class="form-group has-float-label col-sm-6">
	<label for="email">الوصف عربى</label>
    {{ Form::text('description_ar', old('dessertsizes->description_ar'), ['placeholder' => 'الوصف عربى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('description_ar') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('description_ar') ? $errors->first('description_ar') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
	<label for="email">الوصف انجليزى</label>
    {{ Form::text('description_en', old('dessertsizes->description_en'), ['placeholder' => 'الوصف انجليزى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('description_en') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('description_en') ? $errors->first('description_en') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
	<label for="email">السعر</label>
    {{ Form::number('price', old('dessertsizes->price'), ['placeholder' => 'السعر', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('price') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('price') ? $errors->first('price') : '' }}</small>
</div>

<div class="form-group has-float-label col-sm-6">
	<label for="email">وقت الطهى</label>
    {{ Form::number('cock_time', old('dessertsizes->cock_time'), ['placeholder' => 'وقت الطهى بالدقيقة', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('cock_time') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('cock_time') ? $errors->first('cock_time') : '' }}</small>
</div>
<div class="form-group has-float-label col-sm-6">
    <label for="email">اضافة صورة</label>
    {{ Form::file('image', old('dessertsizes->image'), ['placeholder' => 'الصورة', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('image') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('image') ? $errors->first('image') : '' }}</small>
</div>
</div><!--col-xs-12-->
<div class="col-xs-12"> 
<div class="form-group col-sm-12 submit pull-left">
    <p>{{ Form::submit($btn , ['class' => 'btn btn-lg col-sm-3 pull-left btn-primary' . $classes ]) }}
</div>
</div><!--col-xs-12-->