@extends ('layouts.master')
@section('title', 'اضافات')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">

<div class="row">
<div class="col-xs-12">

<div class="row">
<div class="col-xs-12">

		<div class="page-header">
		@if(Route::current()->getName() == 'addonAdd')
		<h1><i class="menu-icon fa fa-magic"></i> اضافة اضافة</h1>
		@elseif (Route::current()->getName() == 'cakeAdd')
		<h1><i class="menu-icon fa fa-magic"></i> اضافة كيك</h1>
		@elseif (Route::current()->getName() == 'sauceAdd')
		<h1><i class="menu-icon fa fa-magic"></i> اضافة صوص</h1>
		@endif
      	</div>

		@if(Route::current()->getName() == 'addonAdd')
			{{ Form::open(['route' => 'addonCreate', 'class' => 'form']) }}
				@include('desserts.extraAddEditForm', ['btn' => 'حفظ', 'classes' => 'btn btn-primary'])
			{{ Form::close() }}
		@elseif (Route::current()->getName() == 'cakeAdd')
			{{ Form::open(['route' => 'cakeCreate', 'class' => 'form']) }}
				@include('desserts.extraAddEditForm', ['btn' => 'حفظ', 'classes' => 'btn btn-primary'])
			{{ Form::close() }}
		@elseif (Route::current()->getName() == 'sauceAdd')
			{{ Form::open(['route' => 'sauceCreate', 'class' => 'form']) }}
				@include('desserts.extraAddEditForm', ['btn' => 'حفظ', 'classes' => 'btn btn-primary'])
			{{ Form::close() }}
		@endif

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!-- /.page-content -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection