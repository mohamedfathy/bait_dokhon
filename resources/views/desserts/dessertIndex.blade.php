@extends ('layouts.master')
@section('title', 'الاصناف')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
  <div class="page-header">
      <h1><i class="fa fa-birthday-cake"></i> الاصناف </h1>
  </div><!--page-header-->
  <div class="row">
  <div class="col-xs-12">
  
<div class="row">
<div class="col-xs-12">
	<div class="page-header">
		<a href="/desserts/dessertAdd" class="btn btn-primary btn-sm">اضافة صنف</a>
	</div>
		
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>الاسم عربى</th>
			<th>الاسم انجليزى</th>
            <th class="center">عرض</th>
			<th class="center">تعديل</th>
			<th class="center">حذف</th>
			</tr>
		</thead>
		
		<tbody>
			@foreach ($Dessert as $D)
			<tr> 
			<td>{{$D->name_ar}}</td>
			<td>{{$D->name_en}}</td>
            <td class="center"><a href="/desserts/dessertShow/{{$D->id}}" class="btn btn-warning btn-xs">عرض</a></td>
			<td class="center"><a href="/desserts/dessertEdit/{{$D->id}}" class="btn btn-success btn-xs">تعديل</a></td>
			<td class="center"><a href="/desserts/dessertDestroy/{{$D->id}}" class="btn btn-danger btn-xs">حذف</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
  	<div class="paginate">
        {{ $Dessert->links() }}
    </div>
	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->


</div><!--col-xs-12-->
</div><!--row-->

</div><!-- /.page-content -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection