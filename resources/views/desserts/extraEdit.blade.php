@extends ('layouts.master')
@section('title', 'تعديل')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">

<div class="row">
<div class="col-xs-12">

<div class="row">
<div class="col-xs-12">

		<div class="page-header">
		@if (Route::current()->getName() == 'addonEdit')
		<h1><i class="menu-icon fa fa-magic"></i> تعديل اضافة</h1>
		@elseif (Route::current()->getName() == 'cakeEdit')
		<h1><i class="menu-icon fa fa-magic"></i> تعديل كيكة</h1>
		@elseif (Route::current()->getName() == 'sauceEdit')
		<h1><i class="menu-icon fa fa-magic"></i> تعديل صوص</h1>
		@endif
      	</div>

		@if(Route::current()->getName() == 'addonEdit')
		{{ Form::model($Extra, ['route' => ['addonUpdate' , $Extra->id], 'class' => 'form', 'method' => 'PUT']) }}
			@include('desserts.extraAddEditForm', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'Extra' => $Extra])
		{{ Form::close() }}
		@elseif (Route::current()->getName() == 'cakeEdit')
		{{ Form::model($Extra, ['route' => ['cakeUpdate' , $Extra->id], 'class' => 'form', 'method' => 'PUT']) }}
			@include('desserts.extraAddEditForm', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'Extra' => $Extra])
		{{ Form::close() }}
		@elseif (Route::current()->getName() == 'sauceEdit')
		{{ Form::model($Extra, ['route' => ['sauceUpdate' , $Extra->id], 'class' => 'form', 'method' => 'PUT']) }}
			@include('desserts.extraAddEditForm', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'Extra' => $Extra])
		{{ Form::close() }}
		@endif

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!-- /.page-content -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection