@extends ('layouts.master')
@section('title', 'تعديل حجم صنف')
@section ('content')



<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
  			<div class="page-header">
     			 <h1><i class="fa fa-birthday-cake"></i>تعديل حجم صنف:</h1>
			
            

            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#438EB9; margin-top2:15px; color:white ">تعديل حجم صنف</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/desertsSize/update/{{$Dessertsize->id}}/{{$Dessertsize->desserts_id}}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                    

                        <div class="form-group{{ $errors->has('description_ar') ? ' has-error' : '' }}">
                            <label for="description_ar" class="col-md-2 control-label">وصف بالعربي:</label>

                            <div class="col-md-8">
                               	        <textarea name="description_ar" class="form-control">{{$Dessertsize->description_ar}}</textarea>

                                @if ($errors->has('description_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description_ar') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        
                             <div class="form-group{{ $errors->has('description_en') ? ' has-error' : '' }}">
                            <label for="description_ar" class="col-md-2 control-label">وصف  بالانجليزية:</label>

                            <div class="col-md-8">
                               	        <textarea name="description_en" class="form-control">{{$Dessertsize->description_en}}</textarea>

                                @if ($errors->has('description_en'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description_en') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        
                        
                                                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-2 control-label">صورة الصنف </label>

                            <div class="col-md-8">
                                <input id="name" type="file" class="form-control" name="image" value="{{ old('name') }}" >

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>






			                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
			                            <label for="price" class="col-md-2 control-label">السعر</label>

			                            <div class="col-md-8">
			                                <input id="price" type="text" value="{{$Dessertsize->price}}" required class="form-control" name="price" >

			                                @if ($errors->has('price'))
			                                    <span class="help-block">
			                                        <strong>{{ $errors->first('price') }}</strong>
			                                    </span>
			                                @endif
			                            </div>
			                        </div>
                        
                        
                        
                        
                        
                                  <div class="form-group{{ $errors->has('cock_time') ? ' has-error' : '' }}">
			                            <label for="cock_time" class="col-md-2 control-label">وقت الاعداد</label>

			                            <div class="col-md-8">
			                                <input id="cock_time" type="text" value="{{$Dessertsize->cock_time}}" required class="form-control" name="cock_time" >

			                                @if ($errors->has('cock_time'))
			                                    <span class="help-block">
			                                        <strong>{{ $errors->first('cock_time') }}</strong>
			                                    </span>
			                                @endif
			                            </div>
			                        </div>
                                    
                                    
                                    
                                    
                                    
                        
                        
                        

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                		تعديل   
                                </button>

                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        
       						
            
            
            <div>
		<div>
	<div>
</div>
@endsection