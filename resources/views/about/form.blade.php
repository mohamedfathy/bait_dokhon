<div class="form-group has-float-label">
	<label for="text">عن الموقع عربى</label>
    {{ Form::text('about_us_ar', old('about_us_ar'), ['placeholder' => 'عن الموقع عربى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('about_us_ar') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('about_us_ar') ? $errors->first('about_us_ar') : '' }}</small>
</div>

<div class="form-group has-float-label">
	<label for="text">عن الموقع انجليزى</label>
    {{ Form::text('about_us_en', old('about_us_en'), ['placeholder' => 'عن الموقع انجليزى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('about_us_en') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('about_us_en') ? $errors->first('about_us_en') : '' }}</small>
</div>

<div class="form-group submit pull-left">
    <p>{{ Form::submit($btn , ['class' => 'btn btn-lg pull-left btn-primary' . $classes ]) }}
</div>