@extends ('layouts.master')
@section('title', 'عن الموقع')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
<div class="page-header">
	<h1><i class="menu-icon fa fa-exclamation-circle"></i> عن الموقع </h1>
</div>


<div class="row">
<div class="col-xs-12">

<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">	
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>عن الموقع عربى</th>
			<th>عن الموقع انجليزى</th>
			<th>تعديل</th>
			</tr>
		</thead>
		
		<tbody>
			<tr>
			<td>{{$Appsetting->about_us_ar}}</td>
			<td>{{$Appsetting->about_us_en}}</td>
			<td><a href="/about/aboutEdit/{{$Appsetting->id}}" class="btn btn-success btn-xs">تعديل</a></td>
			</tr>
		</tbody>
	</table>
</div><!-- /.col-md-12 col-sm-12 col-xs-12 -->

</div><!-- /.row -->
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!--/.page-content-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection