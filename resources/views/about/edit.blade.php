@extends ('layouts.master')
@section('title', 'تعديل عن الموقع')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">

<div class="row">
<div class="col-xs-12">

	<div class="row">
     <div class="col-xs-12">
		<div class="page-header">
      		<h1><i class="menu-icon fa fa-magic"></i> تعديل عن الموقع</h1>
      	</div>
			{{ Form::model($Appsetting, ['route' => ['aboutUpdate' , $Appsetting->id], 'class' => 'form', 'method' => 'PUT']) }}
			@include('about.form', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'Appsetting' => $Appsetting])
			{{ Form::close() }}
	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!-- /.page-content-->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection