@extends ('layouts.master')
@section('title', 'اتصل بنا')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
<div class="page-header">
	<h1><i class="menu-icon fa fa-phone"></i> اتصل بنا </h1>

</div>


<div class="row">
<div class="col-xs-12">

<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="page-header">
		<a href="/contact/phoneAdd" class="btn btn-primary btn-sm">اضافة رقم تليفون</a>
	</div>
		
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>تليفونات الموقع</th>
			<th>حذف</th>
			</tr>
		</thead>
		
		<tbody>
			@foreach ($Contact as $C) 
			<tr>
			<td>{{$C->phone}}</td>
			<td><a href="/contact/phoneDestroy/{{$C->id}}" class="btn btn-danger btn-xs">حذف</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div><!-- /.col-xs-6 -->


<div class="col-md-12 col-sm-12 col-xs-12">
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>

			<th>ايميل الموقع</th>
			<th>تعديل</th>
			</tr>
		</thead>
		
		<tbody>
			<tr>
			<td>{{$Appsetting->contact_email}}</td>
			<td><a href="/contact/emailEdit/{{$Appsetting->id}}" class="btn btn-success btn-xs">تعديل</a></td>
			</tr>
		</tbody>
	</table>
</div><!-- /.col-xs-6 -->

</div><!-- /.row -->
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!--/.page-content-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection