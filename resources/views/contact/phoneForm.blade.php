<div class="form-group has-float-label">
	<label for="name">	رقم تليفون الموقع</label>
    {{ Form::number('phone', old('phone'), ['placeholder' => 'رقم تليفون الموقع', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('phone') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('phone') ? $errors->first('phone') : '' }}</small>
</div>

<div class="form-group submit pull-left">
    <p>{{ Form::submit($btn , ['class' => 'btn btn-lg pull-left btn-primary' . $classes ]) }}
</div>