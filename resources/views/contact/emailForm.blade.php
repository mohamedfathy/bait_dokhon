<div class="form-group has-float-label">
	<label for="email">ايميل الموقع</label>
    {{ Form::email('contact_email', old('contact_email'), ['placeholder' => 'ايميل الموقع', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('contact_email') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('contact_email') ? $errors->first('contact_email') : '' }}</small>
</div>

<div class="form-group submit pull-left">
    <p>{{ Form::submit($btn , ['class' => 'btn btn-lg pull-left btn-primary' . $classes ]) }}
</div>