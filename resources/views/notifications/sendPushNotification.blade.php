@extends ('layouts.master')
@section('title', 'الإشعارات المخصصة')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
    
@if($errors->any())
<div class="alert alert-danger">{{$errors->first()}}</div>
@endif

@if(session('success'))
    <div class="alert alert-success">
        <strong>{{session('success')}}!</strong> 
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger">
        <strong>{{session('error')}}!</strong> 
    </div>
@endif

    <div class="page-header">
        <h1><i class="menu-icon fa fa-bell"></i> الإشعارات المخصصة </h1>
    </div>
    <!--page-header-->
    
    <button class="btn btn-primary pull-right" style="height:auto;margin-top:20px;" id="toggling-button">
                إلى مجموعة /  مستخدم</button>

    <div class="row">
    <div class="col-xs-12">

        <div id="person-form">
            <form dir="rtl" >
            <!-- Handel the Cross Site Request Forgery -->
            {{ csrf_field() }}
            
                <fieldset>
                    <legend><h4>إلى مستخدم واحد </h4></legend>
                    <label for"phone">رقم الهاتف</label>
                    <input type="text" id="phone" name="phone" placeholder="111222333444555">
                    <button id="searching-button" class="btn btn-success">البحث برقم الهاتف</button>
                </fieldset>
            </form>
            <style>
                #displayPersonInfo > td{
                    /*font-size:18px;*/
                    font-weight:bold;
                }
            </style>
            <table dir="rtl" class="table table-bordered table-striped" >
                <thead>
                     <tr>
                      <th>الاسم</th>
                      <th>نوع الحساب</th>
                      <th>الهاتف</th>
                    </tr>
                </thead>
                <tbody id="displayPersonInfo">
                    
                </tbody>
            </table>
            <div id="displayPersonForm">
                
            </div>
        </div>
        
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>
        $(document).ready(function () {
            $("#searching-button").click(function(event){
                event.preventDefault(); // cancel default behavior
                let phone = $("#phone").val();
                
                //clear ant old results
                $('#displayPersonInfo').html('');

                // alert(phone);
                $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                    });
                $.post( "/getPersonInfo", { phone: phone})
                  .done(function( data ) {
                    // alert( "Data Loaded: " + data );
                //   $.each(data, function(propName, propVal) {
                //         console.log(propName, propVal);
                //     });
                    var htmlCode = 
                        "<td>"+data.name+"</td>"+
                        "<td>"+data.type_ar+"</td>"+
                        "<td>"+data.phone+"</td>";
                    $('#displayPersonInfo').html(htmlCode);

                    let htmlForm =' <form  action="{{route("notification.send")}}" method="POST" dir="rtl" enctype="multipart/form-data">\
                                        {{ csrf_field() }}\
                                        <!-- Handel the Cross Site Request Forgery -->\
                                        <label for"message">الرسالة</label>\
                                        <input style="min-width:600px;" type="text" id="message" name="message" placeholder="نص التنبيه">\
                                        <input type="text" name="TableName" value="'+data.TableName+'" hidden>\
                                        <input type="number" name="phone" value="'+data.phone+'" hidden>\
                                        <button class="btn btn-success"> إرسال الإشعار المخصص</button>\
                                        <br><br>\
                                    </form>';
                        
                    $('#displayPersonForm').html(htmlForm);
                   
                  }); 
                  
            });
                
        });

        </script>

        <div id="group-form">
            <form action="{{route('notification.send')}}" method="POST" dir="rtl" enctype="multipart/form-data">
                <!-- Handel the Cross Site Request Forgery -->
                {{ csrf_field() }}
                
                <fieldset>
                    <legend> <h4>  إلى مجموعة مستخدمين    </h4></legend>
                    <label for"message">الرسالة</label>
                    <br>
                    <textarea rows="5" style="width:600px;"  id="message" name="message">نص التنبيه</textarea>
                    <br><br>
                    
                    <label for"group">المجموعة</label>
                    <select name="group">
                        <option></option>
                        <option label="الكل" value="All">الكل</option>
                        <option label="المندوبين فقط" value="DriversOnly">المندوبين فقط</option>
                        <option label="العملاء فقط" value="UsersOnly">العملاء فقط</option>
                    </select>
                    <button class="btn btn-success">إرسال الإشعار المخصص</button>                
                </fieldset>
            </form>
        </div>


    <meta name="csrf-token" content="{{ csrf_token() }}">
              <script>
        $(document).ready(function () {
            $("#test-button").click(function(event){

                $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                    });
                $.post( "/api/test")
                  .done(function( data ) {
                    alert(data);
                 }); 
                  
            });
                
        });

        </script>  
        <script>
            $(document).ready(function() {
                    $("#person-form").hide();
                    $("#toggling-button").click(function() {
                    $("#person-form").toggle('2');
                    $("#group-form").toggle('2');
                });
            });
        </script>
        
    </div>
    <!--col-xs-12-->
    </div>
    <!--row-->

</div><!-- /.page-content -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection