@extends ('layouts.master')
@section('title', 'ضريبة القيمة المضافة')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="page-content">
<div class="page-header">
	<h1><i class="menu-icon fa fa-exclamation-circle"></i>  ضريبة القيمة المضافة </h1>
</div>


<div class="row">
<div class="col-xs-12">

<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">	
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>الضريبة </th>
			<th>تعديل</th>
			</tr>
		</thead>
		
		<tbody>
			<tr>
			<td>{{$Appsetting->fees}}</td>
			<td><a href="/fees/feesEdit/{{$Appsetting->id}}" class="btn btn-success btn-xs">تعديل</a></td>
			</tr>
		</tbody>
	</table>
</div><!-- /.col-md-12 col-sm-12 col-xs-12 -->

</div><!-- /.row -->
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!--/.page-content-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection