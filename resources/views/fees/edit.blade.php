@extends ('layouts.master')
@section('title', 'تعديل ضريبقة القيمة المضافة')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">

<div class="row">
<div class="col-xs-12">

	<div class="row">
     <div class="col-xs-12">
		<div class="page-header">
      		<h1><i class="menu-icon fa fa-magic"></i> تعديل الضريبة </h1>
      	</div>
			{{ Form::model($Appsetting, ['route' => ['feesUpdate' , $Appsetting->id], 'class' => 'form', 'method' => 'PUT']) }}
			@include('fees.form', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'Appsetting' => $Appsetting])
			{{ Form::close() }}
	</div><!-- /.col-xs-12 -->
	</div><!-- /.row -->

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

</div><!-- /.page-content-->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection