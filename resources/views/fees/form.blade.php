
<div class="form-group has-float-label">
	<label for="text">ضريبة القيمة المضافة</label>
    {{ Form::number('fees', old('fees'), ['placeholder' => 'ضريبة القيمة المضافة', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('fees') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('fees') ? $errors->first('fees') : '' }}</small>
</div>

<div class="form-group submit pull-left">
    <p>{{ Form::submit($btn , ['class' => 'btn btn-lg pull-left btn-primary' . $classes ]) }}
</div>